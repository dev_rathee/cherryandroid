package beans;

import android.graphics.Bitmap;

/**
 * Created by devrath.rathee on 3/14/2016.
 */
public class SelectedInspection {

    String title,desc,id,comment,quantity,image;
    Bitmap bitmap;

    public SelectedInspection(String title, String desc, String id, String comment, String quantity, String image, Bitmap bitmap) {
        this.title = title;
        this.desc = desc;
        this.id = id;
        this.comment = comment;
        this.quantity = quantity;
        this.image = image;
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
