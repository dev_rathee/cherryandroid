package beans;

/**
 * Created by devrath.rathee on 5/19/2016.
 */
public class DeviceBean {
    String id,device_type_name,subscription_model_id,price;

    public DeviceBean(String id, String device_type_name, String subscription_model_id, String price) {
        this.id = id;
        this.device_type_name = device_type_name;
        this.subscription_model_id = subscription_model_id;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDevice_type_name() {
        return device_type_name;
    }

    public void setDevice_type_name(String device_type_name) {
        this.device_type_name = device_type_name;
    }

    public String getSubscription_model_id() {
        return subscription_model_id;
    }

    public void setSubscription_model_id(String subscription_model_id) {
        this.subscription_model_id = subscription_model_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
