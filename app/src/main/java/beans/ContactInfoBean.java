package beans;

/**
 * Created by devrath.rathee on 4/18/2016.
 */
public class ContactInfoBean {
    boolean status;
    String message;
    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

   public static class Data{
       String  id,email,name,password,designation,profile_image_path,fname,lname,gender,address,zipcode,city,state,country,phone_no,card_no,card_expiry_date,card_security_code,created_at,created_by,updated_by,updated_at;

       public String getId() {
           return id;
       }

       public void setId(String id) {
           this.id = id;
       }

       public String getEmail() {
           return email;
       }

       public void setEmail(String email) {
           this.email = email;
       }

       public String getName() {
           return name;
       }

       public void setName(String name) {
           this.name = name;
       }

       public String getPassword() {
           return password;
       }

       public void setPassword(String password) {
           this.password = password;
       }

       public String getDesignation() {
           return designation;
       }

       public void setDesignation(String designation) {
           this.designation = designation;
       }

       public String getProfile_image_path() {
           return profile_image_path;
       }

       public void setProfile_image_path(String profile_image_path) {
           this.profile_image_path = profile_image_path;
       }

       public String getFname() {
           return fname;
       }

       public void setFname(String fname) {
           this.fname = fname;
       }

       public String getLname() {
           return lname;
       }

       public void setLname(String lname) {
           this.lname = lname;
       }

       public String getGender() {
           return gender;
       }

       public void setGender(String gender) {
           this.gender = gender;
       }

       public String getAddress() {
           return address;
       }

       public void setAddress(String address) {
           this.address = address;
       }

       public String getZipcode() {
           return zipcode;
       }

       public void setZipcode(String zipcode) {
           this.zipcode = zipcode;
       }

       public String getCity() {
           return city;
       }

       public void setCity(String city) {
           this.city = city;
       }

       public String getState() {
           return state;
       }

       public void setState(String state) {
           this.state = state;
       }

       public String getCountry() {
           return country;
       }

       public void setCountry(String country) {
           this.country = country;
       }

       public String getPhone_no() {
           return phone_no;
       }

       public void setPhone_no(String phone_no) {
           this.phone_no = phone_no;
       }

       public String getCard_no() {
           return card_no;
       }

       public void setCard_no(String card_no) {
           this.card_no = card_no;
       }

       public String getCard_expiry_date() {
           return card_expiry_date;
       }

       public void setCard_expiry_date(String card_expiry_date) {
           this.card_expiry_date = card_expiry_date;
       }

       public String getCard_security_code() {
           return card_security_code;
       }

       public void setCard_security_code(String card_security_code) {
           this.card_security_code = card_security_code;
       }

       public String getCreated_at() {
           return created_at;
       }

       public void setCreated_at(String created_at) {
           this.created_at = created_at;
       }

       public String getCreated_by() {
           return created_by;
       }

       public void setCreated_by(String created_by) {
           this.created_by = created_by;
       }

       public String getUpdated_by() {
           return updated_by;
       }

       public void setUpdated_by(String updated_by) {
           this.updated_by = updated_by;
       }

       public String getUpdated_at() {
           return updated_at;
       }

       public void setUpdated_at(String updated_at) {
           this.updated_at = updated_at;
       }
   }
}
