package beans;

import java.util.ArrayList;

/**
 * Created by devrath.rathee on 4/17/2016.
 */
public class SubscriptionBeanBranch {
    boolean status;
    String message;
    Data data;


    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        ArrayList<SubscriptionDetails> subscription_details;
        ArrayList<SubscriptionPricingDetails> subscription_pricing_details;

        public ArrayList<SubscriptionDetails> getSubscription_details() {
            return subscription_details;
        }

        public void setSubscription_details(ArrayList<SubscriptionDetails> subscription_details) {
            this.subscription_details = subscription_details;
        }

        public ArrayList<SubscriptionPricingDetails> getSubscription_pricing_details() {
            return subscription_pricing_details;
        }

        public void setSubscription_pricing_details(ArrayList<SubscriptionPricingDetails> subscription_pricing_details) {
            this.subscription_pricing_details = subscription_pricing_details;
        }

        public static class SubscriptionDetails{
            String user_id, total_objects, cost, subscribed_date, renewal_date, subscription_model_id, subscription_type,device_type_id;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getTotal_objects() {
                return total_objects;
            }

            public void setTotal_objects(String total_objects) {
                this.total_objects = total_objects;
            }

            public String getCost() {
                return cost;
            }

            public void setCost(String cost) {
                this.cost = cost;
            }

            public String getSubscribed_date() {
                return subscribed_date;
            }

            public void setSubscribed_date(String subscribed_date) {
                this.subscribed_date = subscribed_date;
            }

            public String getRenewal_date() {
                return renewal_date;
            }

            public void setRenewal_date(String renewal_date) {
                this.renewal_date = renewal_date;
            }

            public String getSubscription_model_id() {
                return subscription_model_id;
            }

            public void setSubscription_model_id(String subscription_model_id) {
                this.subscription_model_id = subscription_model_id;
            }

            public String getDevice_type_id() {
                return device_type_id;
            }

            public void setDevice_type_id(String device_type_id) {
                this.device_type_id = device_type_id;
            }

            public String getSubscription_type() {
                return subscription_type;
            }

            public void setSubscription_type(String subscription_type) {
                this.subscription_type = subscription_type;
            }
        }

        public static class SubscriptionPricingDetails{
               String id,subscription_model_id,price,yearly_discount;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getSubscription_model_id() {
                return subscription_model_id;
            }

            public void setSubscription_model_id(String subscription_model_id) {
                this.subscription_model_id = subscription_model_id;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getYearly_discount() {
                return yearly_discount;
            }

            public void setYearly_discount(String yearly_discount) {
                this.yearly_discount = yearly_discount;
            }
        }
    }
}
