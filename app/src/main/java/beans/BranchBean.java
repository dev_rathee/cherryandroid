package beans;

/**
 * Created by devrath.rathee on 5/19/2016.
 */
public class BranchBean {
    String id,subscription_model_id,price,yearly_discount;

    public BranchBean(String id, String subscription_model_id, String price, String yearly_discount) {
        this.id = id;
        this.subscription_model_id = subscription_model_id;
        this.price = price;
        this.yearly_discount = yearly_discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubscription_model_id() {
        return subscription_model_id;
    }

    public void setSubscription_model_id(String subscription_model_id) {
        this.subscription_model_id = subscription_model_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getYearly_discount() {
        return yearly_discount;
    }

    public void setYearly_discount(String yearly_discount) {
        this.yearly_discount = yearly_discount;
    }
}
