package Utils;

/**
 * Created by devrath.rathee on 3/3/2016.
 */
public class Constants {

    public static final String LOGIN_EMAIL_ID = "email";
    public static final String LOGIN_PASSWORD = "password";

    public static final String USER_ID = "user_id";
    public static final String BRANCH_ID = "branch_id";
    public static final String LOGIN_STATUS = "login_status";
    public static final String LOGIN_SUCCESS = "login_success";

    public static final String SAVE_CUSTOMER_CUSTOMERID = "customer_id";
    public static final String SAVE_CUSTOMER_CARID = "car_id";
    public static final String SAVE_CUSTOMER_NAME = "name";
    public static final String SAVE_CUSTOMER_STATE = "state";
    public static final String SAVE_CUSTOMER_COUNTRY = "country";
    public static final String SAVE_CUSTOMER_LICENSE = "licence";
    public static final String SAVE_CUSTOMER_BRANCHID = "branch_id";
    public static final String SAVE_CUSTOMER_CREATEDBY = "created_by";
    public static final String SAVE_CUSTOMER_VIN = "vin";
    public static final String SAVE_CUSTOMER_MAKEID = "make_id";
    public static final String SAVE_CUSTOMER_MODELID = "model_id";
    public static final String SAVE_CUSTOMER_YEAR = "year";
    public static final String SAVE_CUSTOMER_MILEAGE = "mileage";

    public static final String SERVICE_POSITION_ID ="position_id";


    public static final String CUSTOMER_SAVE = "customer_save";

    public static final String INSPECTIONS_LIST_FROM = "inspection_list_from";
    public static final String FROM_DASHBOARD = "from_dashboard";
    public static final String FROM_CUSTOMER_INFO = "from_customer_info";

    public static String CURRENT_VIN = "";

    public static  String CUSTOMER_INFO_FROM = "customer_info_from";
    public static final String FROM_SCAN_VIN = "from_scan_vin";
    public static final String FROM_PENDING_INSPECTION = "from_pending";

    public static final String USER_ID_CHANGE_PASSWORD = "user_id";
    public static final String OLD_PASSWORD_CHANGE_PASSWORD = "old_passwd";
    public static final String NEW_PASSWORD_CHANGE_PASSWORD = "new_passwd";

    public static final String USER_FIRST_NAME = "fname";
    public static final String USER_LAST_NAME = "lname";
    public static final String USER_GENDER = "gender";
    public static final String USER_COUNTRY = "country";
    public static final String USER_ZIPCODE ="zipcode";
    public static final String USER_ADDRESS = "address";
    public static final String USER_CITY = "city";
    public static final String USER_STATE = "state";
    public static final String USER_PHONE_NUMBER = "phone_no";
    public static final String USER_CARD_NO = "card_no";
    public static final String USER_CARD_EXPIRY_DATE = "card_expiry_date";
    public static final String USER_CARD_SECURITY_CODE = "card_security_code";

    public static final String SUBSCRIPTION_INTENT_FILTER = "subs_intent_filter";
    public static final String SUBSCRIPTION_MESSAGE = "subs_message";
    public static final String TRANSACTION_ID = "transaction_id";

}
