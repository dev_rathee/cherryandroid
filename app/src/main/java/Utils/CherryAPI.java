package Utils;

/**
 * Created by devrath.rathee on 3/3/2016.
 */
public interface CherryAPI {

    //String OLD_ROOT_URL = "http://apocrypto.com/cherry/api";
    String ROOT_URL = "http://cherryinspect.com/api";
    String LOGIN_URL = "/user/validate";
    String SIGNUP_URL = "/user/signup";
    String SEARCH_VIN_URL = "/customer/searchvin";
    String SAVE_CUSTOMER_INFO = "/customer/savecustomer";
 //   String UPDATE_CUSTOMER_INFO = "customer/updatecustomer";
    String SERVICES_AND_CATEGORIES = "/service/servicesandservicecategoriesofposition";
    String SAVE_INSPECTIONS = "/inspection/saveinspection";
    String GET_SUBSCRIPTION_INFO = "/subscription/subscription";
    String GET_SUBSCRIPTION_DETAILS_INFO = "/subscription/subscriptiondetails";
    String CHANGE_PASSWORD_URL = "/user/changepassword";
    String GET_POST_USER_INFO = "/user/userprofile";
    String POST_PAYMENT_DETAILS = "/user/paymentmethoddetails";
    String POST_SUBSCRIPTION = "/subscription/recurringsubscription";

}
