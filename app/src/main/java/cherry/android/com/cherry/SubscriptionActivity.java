package cherry.android.com.cherry;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import Utils.CherryAPI;
import Utils.Connectivity;
import Utils.GsonRequest;
import Utils.Utilities;
import beans.SubscriptionDetails;

public class SubscriptionActivity extends AppCompatActivity implements View.OnClickListener {

    Button backButton, saveButton,saveTextButton;
    TextView headerTextView, yourPlanTextView, subsExpiresOnTextView, subscribedTextView, billingTextView, nextPaymentDateTextView;
    ProgressDialog progressDialog;
    RelativeLayout changeSubscriptionRelativeLayout;
    private String SUBSCRIPTION_TYPE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        initialiseUI();
    }

    private void initialiseUI() {

        SUBSCRIPTION_TYPE = getIntent().getStringExtra("SUBSCRIPTION_TYPE");

        backButton = (Button) findViewById(R.id.cancelButton);
        backButton.setOnClickListener(this);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setVisibility(View.GONE);

        saveTextButton = (Button) findViewById(R.id.saveTextButton);
        saveTextButton.setVisibility(View.GONE);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText("Subscription");

        changeSubscriptionRelativeLayout = (RelativeLayout) findViewById(R.id.changeSubscriptionRelativeLayout);
        changeSubscriptionRelativeLayout.setOnClickListener(this);

        yourPlanTextView = (TextView) findViewById(R.id.yourPlanTextView);
        subsExpiresOnTextView = (TextView) findViewById(R.id.subsExpiresOnTextView);
        subscribedTextView = (TextView) findViewById(R.id.subscribedTextView);
        billingTextView = (TextView) findViewById(R.id.billingTextView);
        nextPaymentDateTextView = (TextView) findViewById(R.id.nextPaymentDateTextView);

        progressDialog = new ProgressDialog(SubscriptionActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);

        if (Connectivity.isConnected(SubscriptionActivity.this))
            getBranchSubscriptionInfo();

        else
            Utilities.internetConnectionError(SubscriptionActivity.this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changeSubscriptionRelativeLayout:
                startActivity(new Intent(SubscriptionActivity.this, ChangeSubscriptionActivity.class).putExtra("SUBSCRIPTION_TYPE",SUBSCRIPTION_TYPE));
                break;

            case R.id.cancelButton :
                finish();
                break;
        }
    }


    private void getBranchSubscriptionInfo() {

        progressDialog.show();

        String url = CherryAPI.ROOT_URL + CherryAPI.GET_SUBSCRIPTION_DETAILS_INFO + "/" + LoginActivity.loginBean.getData().getUser().getId() +
                "/" + SUBSCRIPTION_TYPE;


        GsonRequest<SubscriptionDetails> getSubscriptionDetailsGsonRequest = new GsonRequest<SubscriptionDetails>(Request.Method.GET, url, SubscriptionDetails.class, null, new Response.Listener<SubscriptionDetails>() {
            @Override
            public void onResponse(SubscriptionDetails response) {

                progressDialog.dismiss();

                if (response.getStatus()) {

                    yourPlanTextView.setText(response.getData().get(0).getTotal_objects() + " branches");
                    subsExpiresOnTextView.setText(getFormattedDate(response.getData().get(0).getRenewal_date()));
                    subscribedTextView.setText(getFormattedDate(response.getData().get(0).getSubscribed_date()));

                    if (response.getData().get(0).getSubscription_model_id().equals("1"))
                        billingTextView.setText("$" + response.getData().get(0).getCost() + " monthly");

                    else
                        billingTextView.setText("$" + response.getData().get(0).getCost() + " yearly");

                    nextPaymentDateTextView.setText(getFormattedDate(response.getData().get(0).getRenewal_date()));
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilities.showToast(SubscriptionActivity.this,"Data Not Available");
            }
        });

        getSubscriptionDetailsGsonRequest.setShouldCache(false);
        Utilities.getRequestQueue(SubscriptionActivity.this).add(getSubscriptionDetailsGsonRequest);

    }

    private String getFormattedDate(String date)
    {
        {
            if(!(date == null) && !(date.equals(""))) {
                String[] dateArray = date.split("-");

                String formattedDate = "";

                switch (Integer.parseInt(dateArray[1]))
                {
                    case 1 : formattedDate = "Jan ";
                        break;

                    case 2 : formattedDate = "Feb ";
                        break;

                    case 3 : formattedDate = "March ";
                        break;

                    case 4 : formattedDate = "April ";
                        break;

                    case 5 : formattedDate = "May ";
                        break;

                    case 6 : formattedDate = "June ";
                        break;

                    case 7 : formattedDate = "July ";
                        break;

                    case 8 : formattedDate = "August ";
                        break;

                    case 9: formattedDate = "Sept ";
                        break;

                    case 10: formattedDate = "Oct ";
                        break;

                    case 11: formattedDate = "Nov ";
                        break;

                    case 12 : formattedDate = "Dec ";
                        break;
                }

                return formattedDate + dateArray[2] + "," + dateArray[0];
            }
            return "";
        }
    }



}
