package cherry.android.com.cherry;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import Utils.CherryAPI;
import Utils.Connectivity;
import Utils.Constants;
import Utils.Utilities;

public class LoginSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    Button cancelButton, saveButton,saveTextButton;
    TextView headerTextView;
    EditText oldPasswordEditText, newPasswordEditText;
    RelativeLayout headerRelativeLayout;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_settings);

        initialiseUI();
    }

    private void initialiseUI() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);

        headerRelativeLayout = (RelativeLayout) findViewById(R.id.loginSettingHeader);

        cancelButton = (Button) headerRelativeLayout.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);

        saveButton = (Button) headerRelativeLayout.findViewById(R.id.saveButton);
        saveButton.setVisibility(View.GONE);

        saveTextButton = (Button) findViewById(R.id.saveTextButton);
        saveTextButton.setOnClickListener(this);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText("Profile");

        oldPasswordEditText = (EditText) findViewById(R.id.oldPasswordEditText);
        newPasswordEditText = (EditText) findViewById(R.id.newPasswordEditText);
    }

    private void saveNewPassword() {

        String url = CherryAPI.ROOT_URL + CherryAPI.CHANGE_PASSWORD_URL;

        try {
            JSONObject uploadServicesJsonObject = new JSONObject();
            uploadServicesJsonObject.put(Constants.USER_ID_CHANGE_PASSWORD, LoginActivity.loginBean.getData().getUser().getId());
            uploadServicesJsonObject.put(Constants.OLD_PASSWORD_CHANGE_PASSWORD, oldPasswordEditText.getText().toString().trim());
            uploadServicesJsonObject.put(Constants.NEW_PASSWORD_CHANGE_PASSWORD, newPasswordEditText.getText().toString().trim());

            new JsonObjectResponseAsync(LoginSettingsActivity.this, uploadServicesJsonObject).execute(url);


        } catch (Exception e) {

        }
    }

    private boolean checkValidation() {
        if (oldPasswordEditText.getText().toString().trim().length() > 0) {
            if (newPasswordEditText.getText().toString().trim().length() > 0) {
                if (!(oldPasswordEditText.getText().toString().trim().equals(newPasswordEditText.getText().toString().trim()))) {
                    return true;
                } else {
                    Utilities.showToast(this, "Old Password and New Password cannot be same");
                    return false;
                }

            } else {
                Utilities.showToast(this, "Enter New Password");
                return false;
            }
        } else {
            Utilities.showToast(this, "Enter Old Password");
            return false;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.saveTextButton:
                if (Connectivity.isConnected(LoginSettingsActivity.this)) {
                    if (checkValidation())
                        saveNewPassword();
                } else
                    Utilities.internetConnectionError(LoginSettingsActivity.this);
                break;

            case R.id.cancelButton : finish();
                break;
        }

    }

    public class JsonObjectResponseAsync extends AsyncTask<String, JSONObject, JSONObject> {
        Context mContext;
        JSONObject jsonObject;
        ProgressDialog progressDialog;

        public JsonObjectResponseAsync(Context context, JSONObject jsonObject) {
            this.mContext = context;
            this.jsonObject = jsonObject;
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
        }

        @Override
        protected JSONObject doInBackground(String[] urls) {

            JSONObject responseJsonObject = null;

            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 10000);
                HttpConnectionParams.setSoTimeout(myParams, 10000);

                HttpPost httpPost = new HttpPost(urls[0]);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                StringEntity se = new StringEntity(jsonObject.toString());
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String temp = EntityUtils.toString(httpResponse.getEntity());

                responseJsonObject = new JSONObject(temp);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseJsonObject;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }


        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            try {
                progressDialog.dismiss();
                if (!(response == null)) {
                    if (response.getBoolean("status")) {
                        Toast.makeText(LoginSettingsActivity.this, "Password Updated.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    if (response.get("data").equals("Incorrect old password")) {
                        Toast.makeText(LoginSettingsActivity.this, "Old Password incorrect.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (!response.getBoolean("status")) {
                        Utilities.serverError(LoginSettingsActivity.this);
                        return;
                    }
                }
            } catch (Exception e) {
            }
        }


    }
}
