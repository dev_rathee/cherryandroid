package cherry.android.com.cherry;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import Utils.CherryAPI;
import Utils.Connectivity;
import Utils.Constants;
import Utils.GsonRequest;
import Utils.Utilities;
import beans.ContactInfoBean;

public class PaymentDetailsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Button cancelButton, saveButton, saveTextButton;
    TextView headerTextView;
    EditText creditCardEditText, expirationDateEditText, securityCodeEditText, firstNameEditText, lastNameEditText, addressLine1EditText, addressLine2EditText, zipcodeEditText, cityEditText, stateEditText, phoneNumberEditText;
    Spinner countrySpinner;
    String[] countryArray;
    ArrayAdapter<String> countryArrayAdapter;
    ProgressDialog progressDialog;
    String userId = "", selectedCountry = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        initialiseUI();
    }

    private void initialiseUI() {

        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setVisibility(View.GONE);

        saveTextButton = (Button) findViewById(R.id.saveTextButton);
        saveTextButton.setOnClickListener(this);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText("Payment Method");

        creditCardEditText = (EditText) findViewById(R.id.creditCardEditText);
        expirationDateEditText = (EditText) findViewById(R.id.expirationDateEditText);
        securityCodeEditText = (EditText) findViewById(R.id.securityCodeEditText);
        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        addressLine1EditText = (EditText) findViewById(R.id.addressLine1EditText);
        addressLine2EditText = (EditText) findViewById(R.id.addressLine2EditText);
        zipcodeEditText = (EditText) findViewById(R.id.zipEditText);
        cityEditText = (EditText) findViewById(R.id.cityEditText);
        stateEditText = (EditText) findViewById(R.id.stateEditText);
        phoneNumberEditText = (EditText) findViewById(R.id.phoneNumberEditText);
        countrySpinner = (Spinner) findViewById(R.id.countrySpinner);

        countryArray = getResources().getStringArray(R.array.country_array);
        countryArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, countryArray);
        countryArrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        countrySpinner.setAdapter(countryArrayAdapter);
        countrySpinner.setOnItemSelectedListener(this);

        progressDialog = new ProgressDialog(PaymentDetailsActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);

        if (Connectivity.isConnected(PaymentDetailsActivity.this))
            getUserInfo();

        else
            Utilities.internetConnectionError(PaymentDetailsActivity.this);
    }

    private void getUserInfo() {
        progressDialog.show();

        String Get_User_Info_Url = CherryAPI.ROOT_URL + CherryAPI.GET_POST_USER_INFO + "/" + LoginActivity.loginBean.getData().getUser().getId();

        GsonRequest<ContactInfoBean> paymentDetailsGsonRequest = new GsonRequest<ContactInfoBean>(Request.Method.GET, Get_User_Info_Url, ContactInfoBean.class, null, new Response.Listener<ContactInfoBean>() {
            @Override
            public void onResponse(ContactInfoBean response) {

                progressDialog.dismiss();

                if (response.getStatus()) {
                    userId = response.getData().getId();
                    firstNameEditText.setText(response.getData().getFname());
                    lastNameEditText.setText(response.getData().getLname());
                    creditCardEditText.setText(response.getData().getCard_no());
                    securityCodeEditText.setText(response.getData().getCard_security_code());
                    expirationDateEditText.setText(response.getData().getCard_expiry_date());
                    addressLine1EditText.setText(response.getData().getAddress());
                    zipcodeEditText.setText(response.getData().getZipcode());
                    cityEditText.setText(response.getData().getCity());
                    stateEditText.setText(response.getData().getState());
                    phoneNumberEditText.setText(response.getData().getPhone_no());

                    if (response.getData().getCountry().equals("USA"))
                        countrySpinner.setSelection(0);

                    else if (response.getData().getCountry().equals("CANADA"))
                        countrySpinner.setSelection(1);

                    else if (response.getData().getCountry().equals("MEXICO"))
                        countrySpinner.setSelection(2);
                } else
                    Toast.makeText(PaymentDetailsActivity.this, "Data Unavailable.", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilities.serverError(PaymentDetailsActivity.this);
            }
        });

        paymentDetailsGsonRequest.setShouldCache(false);
        Utilities.getRequestQueue(PaymentDetailsActivity.this).add(paymentDetailsGsonRequest);

    }

    private boolean checkExpirationDateValidation() {
        if (expirationDateEditText.getText().toString().trim().contains("/")) {
            String[] array = expirationDateEditText.getText().toString().trim().split("/");

            if (array.length > 1) {
                if (!(Integer.parseInt(array[0]) > 12 || Integer.parseInt(array[0]) == 0)) {
                    if (!(Integer.parseInt(array[1]) < 16 || Integer.parseInt(array[0]) == 0)) {
                        return true;
                    } else {
                        Utilities.showToast(this, "Enter Proper Year");
                        return false;
                    }
                } else {
                    Utilities.showToast(this, "Enter Proper Month");
                    return false;
                }
            } else {
                Utilities.showToast(this, "Enter Proper Expiration Date");
                return false;
            }
        } else {
            Utilities.showToast(this, "Enter Expiration in format MM/YY");
            return false;
        }

    }

    private boolean checkValidation() {
        if (creditCardEditText.getText().toString().trim().length() > 0) {
            if (creditCardEditText.getText().toString().trim().length() == 16) {
                if (expirationDateEditText.getText().toString().trim().length() > 0) {
                    if (securityCodeEditText.getText().toString().trim().length() > 0) {
                        if (securityCodeEditText.getText().toString().trim().length() == 3) {
                            if (firstNameEditText.getText().toString().trim().length() > 0) {
                                if (lastNameEditText.getText().toString().trim().length() > 0) {
                                    if (addressLine1EditText.getText().toString().trim().length() > 0) {
                                        if (zipcodeEditText.getText().toString().trim().length() > 0) {
                                            if (cityEditText.getText().toString().trim().length() > 0) {
                                                if (stateEditText.getText().toString().trim().length() > 0) {
                                                    if (phoneNumberEditText.getText().toString().trim().length() > 0) {
                                                        if (checkExpirationDateValidation())
                                                            return true;

                                                        else
                                                            return false;
                                                    } else {
                                                        Utilities.showToast(this, "Enter Phone Number");
                                                        return false;
                                                    }
                                                } else {
                                                    Utilities.showToast(this, "Enter State");
                                                    return false;
                                                }
                                            } else {
                                                Utilities.showToast(this, "Enter City");
                                                return false;
                                            }
                                        } else {
                                            Utilities.showToast(this, "Enter ZipCode");
                                            return false;
                                        }
                                    } else {
                                        Utilities.showToast(this, "Enter Address Line 1");
                                        return false;
                                    }
                                } else {
                                    Utilities.showToast(this, "Enter Last Name");
                                    return false;
                                }
                            } else {
                                Utilities.showToast(this, "Enter First Name");
                                return false;
                            }
                        } else {
                            Utilities.showToast(this, "Enter 3 digit Security Code");
                            return false;
                        }
                    } else {
                        Utilities.showToast(this, "Enter Security Code");
                        return false;
                    }
                } else {
                    Utilities.showToast(this, "Enter Expiration Data");
                    return false;
                }

            } else {
                Utilities.showToast(this, "Enter 16 digit Credit Card Number");
                return false;
            }
        } else {
            Utilities.showToast(this, "Enter Credit Card Number");
            return false;
        }
    }


    private void savePaymentInfo() {

        if (checkValidation()) {


            String url = CherryAPI.ROOT_URL + CherryAPI.POST_PAYMENT_DETAILS;

            try {
                JSONObject paymentInfoJsonObject = new JSONObject();
                paymentInfoJsonObject.put(Constants.USER_ID_CHANGE_PASSWORD, userId);
                paymentInfoJsonObject.put(Constants.USER_FIRST_NAME, firstNameEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_LAST_NAME, lastNameEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_ADDRESS, addressLine1EditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_ZIPCODE, zipcodeEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_CITY, cityEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_STATE, stateEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_COUNTRY, selectedCountry);
                paymentInfoJsonObject.put(Constants.USER_PHONE_NUMBER, phoneNumberEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_CARD_NO, creditCardEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_CARD_EXPIRY_DATE, expirationDateEditText.getText().toString().trim());
                paymentInfoJsonObject.put(Constants.USER_CARD_SECURITY_CODE, securityCodeEditText.getText().toString().trim());

                new JsonObjectResponseAsync(this, paymentInfoJsonObject).execute(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.saveTextButton:
                savePaymentInfo();
                break;

            case R.id.cancelButton:
                finish();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.countrySpinner)
            selectedCountry = (String) parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public class JsonObjectResponseAsync extends AsyncTask<String, JSONObject, JSONObject> {
        Context mContext;
        JSONObject jsonObject;
        ProgressDialog progressDialog;

        public JsonObjectResponseAsync(Context context, JSONObject jsonObject) {
            this.mContext = context;
            this.jsonObject = jsonObject;
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading");
        }

        @Override
        protected JSONObject doInBackground(String[] urls) {

            JSONObject responseJsonObject = null;

            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 10000);
                HttpConnectionParams.setSoTimeout(myParams, 10000);

                HttpPost httpPost = new HttpPost(urls[0]);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                StringEntity se = new StringEntity(jsonObject.toString());
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String temp = EntityUtils.toString(httpResponse.getEntity());

                responseJsonObject = new JSONObject(temp);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseJsonObject;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }


        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            try {
                progressDialog.dismiss();
                if (!(response == null)) {
                    if (response.getBoolean("status")) {
                        Toast.makeText(PaymentDetailsActivity.this, "Info Updated", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Utilities.serverError(PaymentDetailsActivity.this);
                        return;
                    }
                }
            } catch (Exception e) {
            }
        }


    }
}
