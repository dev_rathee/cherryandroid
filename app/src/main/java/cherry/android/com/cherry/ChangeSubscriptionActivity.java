package cherry.android.com.cherry;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.DecimalFormat;
import java.util.ArrayList;

import Utils.CherryAPI;
import Utils.Constants;
import Utils.GsonRequest;
import Utils.Utilities;
import beans.BranchBean;
import beans.ContactInfoBean;
import beans.DeviceBean;
import jncryptor.AES256JNCryptor;
import jncryptor.JNCryptor;

public class ChangeSubscriptionActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Button backButton, saveButton, saveTextButton, changeSubscriptionButton;
    TextView headerTextView, totalPriceSubscriptionTextView, billingBottomTextView, numberOfBranchesBottomTextView, numberOfBranchesTextView;
    EditText numberOfBranchesEditText;
    Spinner billingCycleSpinner, deviceTypeSpinner;
    public static ContactInfoBean contactInfoBean;
    ArrayAdapter<String> billingCyclerAdapter, deviceTypeAdapter;
    String SUBSCRIPTION_TYPE = "", BILLING_CYCLE, SELECTED_DEVICE = "", SUBSCRIPTION_MODEL_ID = "1", DEVICE_TYPE_ID = "0", TOTAL_OBJECTS = "0";
    RelativeLayout deviceTypeRelativeLayout;
    ArrayList<BranchBean> branchBeanArrayList;
    ArrayList<DeviceBean> deviceBeanArrayList;
    ArrayList<String> deviceArrayList;
    float currentPrice;
    static float totalBillingPrice = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_subscription);

        initialiseUI();
        getUserInfo();

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(Constants.SUBSCRIPTION_INTENT_FILTER));
    }

    private void initialiseUI() {

        if (branchBeanArrayList == null)
            branchBeanArrayList = new ArrayList<>();

        if (deviceBeanArrayList == null)
            deviceBeanArrayList = new ArrayList<>();

        SUBSCRIPTION_TYPE = getIntent().getStringExtra("SUBSCRIPTION_TYPE");

        billingCycleSpinner = (Spinner) findViewById(R.id.billingCycleSpinner);

        billingCyclerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.billing_cycle_array));
        billingCyclerAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        billingCycleSpinner.setAdapter(billingCyclerAdapter);
        billingCycleSpinner.setOnItemSelectedListener(this);

        backButton = (Button) findViewById(R.id.cancelButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setVisibility(View.GONE);

        saveTextButton = (Button) findViewById(R.id.saveTextButton);
        saveTextButton.setVisibility(View.GONE);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText("Change Subscription");

        numberOfBranchesTextView = (TextView) findViewById(R.id.numberOfBranchesTextView);

        billingBottomTextView = (TextView) findViewById(R.id.billingBottomTextView);
        numberOfBranchesBottomTextView = (TextView) findViewById(R.id.numberOfBranchesBottomTextView);

        numberOfBranchesEditText = (EditText) findViewById(R.id.numberOfBranchesEditText);
        numberOfBranchesEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if ((s.toString().trim().equals("") || s.toString().equals("0"))) {
                    numberOfBranchesBottomTextView.setText("NA");
                    totalPriceSubscriptionTextView.setText("NA");
                } else {
                    if (SUBSCRIPTION_TYPE.equals("1"))
                        numberOfBranchesBottomTextView.setText(s.toString() + " branches");

                    else
                        numberOfBranchesBottomTextView.setText(s.toString() + " devices");

                    if (BILLING_CYCLE.equals("Monthly")) {
                        totalPriceSubscriptionTextView.setText("$ " + (Integer.parseInt(s.toString()) * currentPrice));
                        totalBillingPrice = (Integer.parseInt(s.toString()) * currentPrice);
                    } else {
                        totalPriceSubscriptionTextView.setText("$ " + (Integer.parseInt(s.toString()) * currentPrice * 12));
                        totalBillingPrice = (Integer.parseInt(s.toString()) * currentPrice * 12);
                    }
                }
            }
        });

        totalPriceSubscriptionTextView = (TextView) findViewById(R.id.totalPriceSubscriptionTextView);

        if (SUBSCRIPTION_TYPE.equals("2")) {
            deviceTypeSpinner = (Spinner) findViewById(R.id.deviceTypeSpinner);
            deviceTypeSpinner.setOnItemSelectedListener(this);
            deviceTypeRelativeLayout = (RelativeLayout) findViewById(R.id.deviceTypeRelativeLayout);

            numberOfBranchesTextView.setText("Number of Devices");

            deviceTypeRelativeLayout.setVisibility(View.VISIBLE);

            if (deviceArrayList == null)
                deviceArrayList = new ArrayList<>();

        }

        new BranchDeviceAsync(ChangeSubscriptionActivity.this).execute(CherryAPI.ROOT_URL + CherryAPI.GET_SUBSCRIPTION_INFO + "/" + LoginActivity.loginBean.getData().getUser().getId() +
                "/" + SUBSCRIPTION_TYPE);

        changeSubscriptionButton = (Button) findViewById(R.id.changeSubscriptionButton);
        changeSubscriptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkValidation()) {
                    if (!(numberOfBranchesEditText.getText().toString().trim().equals("") || numberOfBranchesEditText.getText().toString().equals("0"))) {
                        if (!(Integer.parseInt(numberOfBranchesEditText.getText().toString()) == 0)) {
                            Utilities.showToast(ChangeSubscriptionActivity.this, "Transaction in process");
                            startService(new Intent(ChangeSubscriptionActivity.this, AuthorizeNetIntentService.class));
                        } else
                            Utilities.showToast(ChangeSubscriptionActivity.this, "Total Price cannot be 0");

                    } else
                        Utilities.showToast(ChangeSubscriptionActivity.this, "Total Price cannot be 0");
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public class BranchDeviceAsync extends AsyncTask<String, Void, JSONObject> {

        Context mContext;


        public BranchDeviceAsync(Context context) {
            this.mContext = context;
        }

        @Override
        protected JSONObject doInBackground(String[] urls) {

            JSONObject responseJsonObject = null;

            try {

                HttpClient httpclient = new DefaultHttpClient();

                HttpGet request = new HttpGet();
                URI website = new URI(urls[0]);
                request.setURI(website);
                HttpResponse response = httpclient.execute(request);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));


                String line = in.readLine();
                responseJsonObject = new JSONObject(line);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseJsonObject;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);

            try {
                Log.i("Response : ", response.toString());

                // progressDialog.dismiss();

                if (response.getBoolean("status")) {
                    JSONObject jsonObject = response.getJSONObject("data");
                    // Object subscriptionDetailsObject = jsonObject.get("subscription_details");
                    Object subscriptionPricingDetailsObject = jsonObject.get("subscription_pricing_details");

                    JSONArray array = (JSONArray) subscriptionPricingDetailsObject;
                    if (array.length() > 0) {
                        if (SUBSCRIPTION_TYPE.equals("1")) {

                            branchBeanArrayList.clear();

                            for (int i = 0; i < array.length(); i++) {
                                branchBeanArrayList.add(new BranchBean(array.getJSONObject(i).getString("id"),
                                        array.getJSONObject(i).getString("subscription_model_id"),
                                        array.getJSONObject(i).getString("price"), array.getJSONObject(i).getString("yearly_discount")));
                            }

                            billingBottomTextView.setText("$ " + branchBeanArrayList.get(0).getPrice() + " branch/month");
                            currentPrice = Float.parseFloat(branchBeanArrayList.get(0).getPrice());

                        } else {

                            deviceBeanArrayList.clear();
                            deviceArrayList.clear();
                            for (int i = 0; i < array.length(); i++) {
                                if (!(array.getJSONObject(i).getString("device_type_name").contains("iPad") || array.getJSONObject(i).getString("device_type_name").contains("iPhone"))) {
                                    deviceBeanArrayList.add(new DeviceBean(array.getJSONObject(i).getString("id"), array.getJSONObject(i).getString("device_type_name"),
                                            array.getJSONObject(i).getString("subscription_model_id"),
                                            array.getJSONObject(i).getString("price")));

                                    deviceArrayList.add(array.getJSONObject(i).getString("device_type_name"));
                                }
                            }

                            billingBottomTextView.setText("$ " + deviceBeanArrayList.get(0).getPrice() + " device/month");
                            currentPrice = Float.parseFloat(deviceBeanArrayList.get(0).getPrice());


                            deviceTypeAdapter = new ArrayAdapter<String>(ChangeSubscriptionActivity.this, android.R.layout.simple_spinner_dropdown_item, deviceArrayList);
                            deviceTypeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                            deviceTypeSpinner.setAdapter(deviceTypeAdapter);
                            //deviceTypeSpinner.setOnItemSelectedListener(this);
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void getUserInfo() {

        String Get_User_Info_Url = CherryAPI.ROOT_URL + CherryAPI.GET_POST_USER_INFO + "/" + LoginActivity.loginBean.getData().getUser().getId();

        GsonRequest<ContactInfoBean> paymentDetailsGsonRequest = new GsonRequest<ContactInfoBean>(Request.Method.GET, Get_User_Info_Url, ContactInfoBean.class, null, new Response.Listener<ContactInfoBean>() {
            @Override
            public void onResponse(ContactInfoBean response) {

                if (response.getStatus())
                    contactInfoBean = response;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        paymentDetailsGsonRequest.setShouldCache(false);
        Utilities.getRequestQueue(ChangeSubscriptionActivity.this).add(paymentDetailsGsonRequest);

    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Utilities.showToast(context, intent.getStringExtra(Constants.SUBSCRIPTION_MESSAGE));
            String transactionId = intent.getStringExtra(Constants.TRANSACTION_ID);
            postSubscriptionDetails(transactionId);

        }
    };

    private void postSubscriptionDetails(String transactionId) {

        TOTAL_OBJECTS = numberOfBranchesEditText.getText().toString().trim();

        new PostSubscriptionDetailsAsync(ChangeSubscriptionActivity.this).execute(CherryAPI.ROOT_URL + CherryAPI.POST_SUBSCRIPTION);
    }

    public class PostSubscriptionDetailsAsync extends AsyncTask<String, Void, JSONObject> {

        Context mContext;


        public PostSubscriptionDetailsAsync(Context context) {
            this.mContext = context;
        }

        @Override
        protected JSONObject doInBackground(String[] urls) {

            JSONObject responseJsonObject = null;

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", LoginActivity.loginBean.getData().getUser().getId());
                jsonObject.put("device_type_id", DEVICE_TYPE_ID);
                jsonObject.put("total_objects", TOTAL_OBJECTS);
                jsonObject.put("cost", twoDecimals(totalBillingPrice) + "");
                jsonObject.put("subscription_model_id", SUBSCRIPTION_MODEL_ID);
                jsonObject.put("subscription_type", SUBSCRIPTION_TYPE);
                jsonObject.put("p1", getJNEncryptedString(contactInfoBean.getData().getCard_no()));
                jsonObject.put("p2", getJNEncryptedString(contactInfoBean.getData().getCard_security_code()));
                jsonObject.put("p3", contactInfoBean.getData().getCard_expiry_date().trim());

                HttpClient httpClient = new DefaultHttpClient();

                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 10000);
                HttpConnectionParams.setSoTimeout(myParams, 10000);

                HttpPost httpPost = new HttpPost(urls[0]);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                StringEntity se = new StringEntity(jsonObject.toString());
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String temp = EntityUtils.toString(httpResponse.getEntity());

                Log.i("Response : ", temp);

                responseJsonObject = new JSONObject(temp);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseJsonObject;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);

            try {

                Log.i("Response : ", response.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private float twoDecimals(float val)
    {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Float.valueOf(twoDForm.format(val));
    }

    private String getJNEncryptedString(String text) {
        JNCryptor cryptor = new AES256JNCryptor();
        byte[] plaintext = text.getBytes();

        String password = "pizzaHutIsTheWorst";

        try {
            byte[] ciphertext = cryptor.encryptData(plaintext, password.toCharArray());
            String cipherTextString = Base64.encodeToString(ciphertext, Base64.DEFAULT);
           /* byte[] decrypted = cryptor.decryptData(ciphertext, password.toCharArray());
            String str = new String(decrypted, "UTF-8");
            Log.i("Str : ",str);*/

            return cipherTextString;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getId() == R.id.billingCycleSpinner) {
            BILLING_CYCLE = (String) parent.getItemAtPosition(position);
            if ((numberOfBranchesEditText.getText().toString().trim().equals("") || numberOfBranchesEditText.getText().toString().equals("0"))) {
                numberOfBranchesBottomTextView.setText("NA");
                totalPriceSubscriptionTextView.setText("NA");
            } else {

                if (SUBSCRIPTION_TYPE.equals("1"))
                    numberOfBranchesBottomTextView.setText(numberOfBranchesEditText.getText().toString().trim() + " branches");

                else
                    numberOfBranchesBottomTextView.setText(numberOfBranchesEditText.getText().toString().trim() + " devices");


                if (BILLING_CYCLE.equals("Monthly")) {
                    SUBSCRIPTION_MODEL_ID = "1";
                    totalPriceSubscriptionTextView.setText("$ " + (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice));
                    totalBillingPrice = (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice);
                } else {
                    SUBSCRIPTION_MODEL_ID = "2";
                    totalPriceSubscriptionTextView.setText("$ " + (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice * 12));
                    totalBillingPrice = (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice * 12);
                }
            }
        } else if (parent.getId() == R.id.deviceTypeSpinner) {

            SELECTED_DEVICE = (String) parent.getItemAtPosition(position);
            for (DeviceBean bean : deviceBeanArrayList) {
                if (bean.getDevice_type_name().equals(SELECTED_DEVICE)) {
                    billingBottomTextView.setText("$ " + bean.getPrice() + " device/month");
                    currentPrice = Float.parseFloat(bean.getPrice());
                    DEVICE_TYPE_ID = bean.getId();
                    break;
                }
            }


            if ((numberOfBranchesEditText.getText().toString().trim().equals("") || numberOfBranchesEditText.getText().toString().equals("0"))) {
                numberOfBranchesBottomTextView.setText("NA");
                totalPriceSubscriptionTextView.setText("NA");
            } else {
                numberOfBranchesBottomTextView.setText(numberOfBranchesEditText.getText().toString().trim() + " devices");

                if (BILLING_CYCLE.equals("Monthly")) {
                    totalPriceSubscriptionTextView.setText("$ " + (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice));
                    totalBillingPrice = (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice);
                } else {
                    totalPriceSubscriptionTextView.setText("$ " + (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice * 12));
                    totalBillingPrice = (Integer.parseInt(numberOfBranchesEditText.getText().toString().trim()) * currentPrice * 12);
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private boolean checkValidation() {
        if (contactInfoBean.getData().getCard_no().trim().length() > 0) {
            if (contactInfoBean.getData().getCard_no().trim().length() == 16) {
                if (contactInfoBean.getData().getCard_expiry_date().trim().length() > 0) {
                    if (contactInfoBean.getData().getCard_security_code().trim().length() > 0) {
                        if (contactInfoBean.getData().getCard_security_code().trim().length() == 3) {
                            if (contactInfoBean.getData().getFname().trim().length() > 0) {
                                if (contactInfoBean.getData().getLname().trim().length() > 0) {
                                    if (contactInfoBean.getData().getAddress().trim().length() > 0) {
                                        if (contactInfoBean.getData().getZipcode().trim().length() > 0) {
                                            if (contactInfoBean.getData().getCity().trim().length() > 0) {
                                                if (contactInfoBean.getData().getState().trim().length() > 0) {
                                                    if (contactInfoBean.getData().getPhone_no().trim().length() > 0) {
                                                        if (checkExpirationDateValidation())
                                                            return true;

                                                        else
                                                            return false;
                                                    } else {
                                                        Utilities.showToast(this, "Phone Number unavailable");
                                                        return false;
                                                    }
                                                } else {
                                                    Utilities.showToast(this, "State unavailable");
                                                    return false;
                                                }
                                            } else {
                                                Utilities.showToast(this, "City unavailable");
                                                return false;
                                            }
                                        } else {
                                            Utilities.showToast(this, "ZipCode unavailable");
                                            return false;
                                        }
                                    } else {
                                        Utilities.showToast(this, "Address unavailable");
                                        return false;
                                    }
                                } else {
                                    Utilities.showToast(this, "Last Name unavailable");
                                    return false;
                                }
                            } else {
                                Utilities.showToast(this, "First Name unavailable");
                                return false;
                            }
                        } else {
                            Utilities.showToast(this, "Security Code should be 3 digits");
                            return false;
                        }
                    } else {
                        Utilities.showToast(this, "Security Code unavailable");
                        return false;
                    }
                } else {
                    Utilities.showToast(this, "Expiration Data unavailable");
                    return false;
                }

            } else {
                Utilities.showToast(this, "Credit Card Number should be 16 digits");
                return false;
            }
        } else {
            Utilities.showToast(this, "Credit Card Number unavailable");
            return false;
        }
    }

    private boolean checkExpirationDateValidation() {
        if (contactInfoBean.getData().getCard_expiry_date().trim().contains("/")) {
            String[] array = contactInfoBean.getData().getCard_expiry_date().trim().split("/");

            if (array.length > 1) {
                if (!(Integer.parseInt(array[0]) > 12 || Integer.parseInt(array[0]) == 0)) {
                    if (!(Integer.parseInt(array[1]) < 16 || Integer.parseInt(array[0]) == 0)) {
                        return true;
                    } else {
                        Utilities.showToast(this, "Year Invalid");
                        return false;
                    }
                } else {
                    Utilities.showToast(this, "Month Invalid");
                    return false;
                }
            } else {
                Utilities.showToast(this, "Expiration Date Invalid");
                return false;
            }
        } else {
            Utilities.showToast(this, "Expiry Date format Invalid");
            return false;
        }

    }

}
