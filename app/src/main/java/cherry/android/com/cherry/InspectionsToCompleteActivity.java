package cherry.android.com.cherry;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapters.InspectionsToCompleteRecyclerViewAdapter;
import Utils.CherryAPI;
import Utils.Connectivity;
import Utils.Constants;
import Utils.GsonRequest;
import Utils.Utilities;
import beans.SearchVinBean;
import beans.SelectedInspection;
import beans.ServiceBean;


public class InspectionsToCompleteActivity extends ActionBarActivity implements View.OnClickListener {

    TextView headerTextView;
    Button saveButton, submitButton, cancelButton;
    RecyclerView inspectionsListRecyclerView;

    InspectionsToCompleteRecyclerViewAdapter adapter;
    static ArrayList<ServiceBean.Services> inspectionServiceArrayList;
    static ArrayList<ServiceBean.Service_Categories> inspectionServiceCategoriesArrayList;

    public static boolean lightInspectionChecked, generalInspectionChecked;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspections_to_complete);

        initialiseUI();
    }


    private void initialiseUI() {

        if (Utilities.getCustomerInfo(Constants.CURRENT_VIN) == null) {
            SearchVinBean.Data data = new SearchVinBean.Data("XXX", Constants.CURRENT_VIN, "XXX", "XXX", "XXX", "XXX",
                    "XXX", DashboardActivity.selectedProcedure, "XXX", "XXX", "XXX", "XXX", DashboardActivity.USER_NAME, "XXX", "XXX",
                    "XXX", "XXX");

            Utilities.saveCustomerInfoHashMap(Constants.CURRENT_VIN, data);
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText(DashboardActivity.USER_NAME);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setText("");
        saveButton.setOnClickListener(this);
        //  saveButton.setBackground(getDrawable(R.drawable.ic_drawer));

        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);

        submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(this);

        inspectionsListRecyclerView = (RecyclerView) findViewById(R.id.inspectionsRecyclerView);
        inspectionsListRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        inspectionsListRecyclerView.setLayoutManager(mLayoutManager);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!(adapter == null))
            adapter.notifyDataSetChanged();

        if (Connectivity.isConnected(InspectionsToCompleteActivity.this))
            getServicesAndCategoriesList();

        else
            Utilities.internetConnectionError(InspectionsToCompleteActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.saveButton) {
            startActivity(new Intent(InspectionsToCompleteActivity.this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }

        if (v.getId() == R.id.cancelButton)
            startActivity(new Intent(this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

        if (v.getId() == R.id.submitButton) {
             if (lightInspectionChecked)
            {
                   if (generalInspectionChecked)
                {
                    if (Constants.CURRENT_VIN.length() == 17) {

                        if (!(Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN) == null)) {

                            if (Connectivity.isConnected(InspectionsToCompleteActivity.this)) {
                                progressDialog.show();
                                new PostInspectionsAsync(InspectionsToCompleteActivity.this).execute(CherryAPI.ROOT_URL + CherryAPI.SAVE_INSPECTIONS);
                            } else
                                Utilities.internetConnectionError(InspectionsToCompleteActivity.this);
                        }
                    } else
                        Utilities.showToast(InspectionsToCompleteActivity.this, "Scan VIN");
                }
                else
                   Toast.makeText(InspectionsToCompleteActivity.this, "Select General Inspections.", Toast.LENGTH_SHORT).show();
            }
             else
               Toast.makeText(InspectionsToCompleteActivity.this, "Select Lights Inspections.", Toast.LENGTH_SHORT).show();
        }
    }


    private void getServicesAndCategoriesList() {
        progressDialog.show();

        lightInspectionChecked = false;
        generalInspectionChecked = false;

        if ((Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN)) != null && Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN).size() > 0) {
            for (SelectedInspection bean : Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN)) {

                if (bean.getQuantity().equals(""))
                    lightInspectionChecked = true;

                else
                    generalInspectionChecked = true;

            }
        }


        if (!(Utilities.getTempArrayList() == null))
            Utilities.clearTempArrayList();


        String url = CherryAPI.ROOT_URL + CherryAPI.SERVICES_AND_CATEGORIES + "?" + Constants.SERVICE_POSITION_ID + "=" + DashboardActivity.selectedProcedure +
                "&" + Constants.SAVE_CUSTOMER_BRANCHID + "=" + LoginActivity.loginBean.getData().getUserBranch().get(0).getId() + "&" +
                Constants.SAVE_CUSTOMER_CREATEDBY + "=" + LoginActivity.loginBean.getData().getUser().getId();

        GsonRequest<ServiceBean> serviceBeanGsonRequest = new GsonRequest<ServiceBean>(Request.Method.GET, url, ServiceBean.class, null, new Response.Listener<ServiceBean>() {
            @Override
            public void onResponse(ServiceBean response) {

                progressDialog.dismiss();
                ServiceBean serviceBean = response;

                if (!(serviceBean.getService_categories() == null)) {
                    if (serviceBean.getService_categories().size() > 0) {
                        if (inspectionServiceCategoriesArrayList == null)
                            inspectionServiceCategoriesArrayList = new ArrayList<>();

                        inspectionServiceCategoriesArrayList.clear();
                        inspectionServiceCategoriesArrayList = serviceBean.getService_categories();

                        if (inspectionServiceCategoriesArrayList.size() > 0) {

                            inspectionServiceCategoriesArrayList.add(new ServiceBean.Service_Categories("1", "Customer Info", "0"));
                            inspectionServiceCategoriesArrayList.add(new ServiceBean.Service_Categories("2", "VIN Scan", "0"));

                            adapter = new InspectionsToCompleteRecyclerViewAdapter(InspectionsToCompleteActivity.this, inspectionServiceCategoriesArrayList);
                            inspectionsListRecyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        }
                    }
                }

                if (!(serviceBean.getServices() == null)) {
                    if (serviceBean.getServices().size() > 0) {
                        if (inspectionServiceArrayList == null)
                            inspectionServiceArrayList = new ArrayList<>();

                        inspectionServiceArrayList.clear();
                        inspectionServiceArrayList = serviceBean.getServices();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilities.serverError(InspectionsToCompleteActivity.this);
            }
        });

        serviceBeanGsonRequest.setShouldCache(false);
        Utilities.getRequestQueue(InspectionsToCompleteActivity.this).add(serviceBeanGsonRequest);
    }


    //AsyncTask

    public class PostInspectionsAsync extends AsyncTask<String, Void, JSONObject> {

        Context mContext;


        public PostInspectionsAsync(Context context) {
            this.mContext = context;
        }

        @Override
        protected JSONObject doInBackground(String[] urls) {

            JSONObject responseJsonObject = null;
            JSONArray jsonArray = null;

            try {

                ArrayList<SelectedInspection> selectedInspectionArrayList = Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN);

                if (selectedInspectionArrayList.size() > 0)
                    jsonArray = new JSONArray();

                for (int i = 0; i < selectedInspectionArrayList.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("service_id", selectedInspectionArrayList.get(i).getId());
                    jsonObject.put("qty", selectedInspectionArrayList.get(i).getQuantity());
                    jsonObject.put("custom_comment", selectedInspectionArrayList.get(i).getComment());
                    jsonObject.put("image", selectedInspectionArrayList.get(i).getImage());

                    jsonArray.put(jsonObject);
                }

                JSONObject uploadServicesJsonObject = new JSONObject();
                uploadServicesJsonObject.put("branch_id", LoginActivity.loginBean.getData().getUserBranch().get(0).getId());
                uploadServicesJsonObject.put("created_by", LoginActivity.loginBean.getData().getUserBranch().get(0).getCreated_by());
                uploadServicesJsonObject.put("car_id", Utilities.getCustomerInfo(Constants.CURRENT_VIN).getId());
                uploadServicesJsonObject.put("services", jsonArray);


                HttpClient httpClient = new DefaultHttpClient();

                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 10000);
                HttpConnectionParams.setSoTimeout(myParams, 10000);


                HttpPost httpPost = new HttpPost(urls[0]);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");


                StringEntity se = new StringEntity(uploadServicesJsonObject.toString());
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String temp = EntityUtils.toString(httpResponse.getEntity());

                Log.i("Response : ", temp);

                responseJsonObject = new JSONObject(temp);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseJsonObject;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);

            try {
                //  if(!(response == null))
                {

                    Log.i("Response : ", response.toString());

                    progressDialog.dismiss();

                    // boolean status = (boolean)response.get("status");
                    // if(status)
                    {
                        Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN).clear();
                        Utilities.customerInfoHashMap.remove(Constants.CURRENT_VIN);

                        Toast.makeText(InspectionsToCompleteActivity.this, "Data Saved", Toast.LENGTH_SHORT).show();

                        // if(transitionFrom.equals(Constants.FROM_CUSTOMER_INFO))
                        startActivity(new Intent(InspectionsToCompleteActivity.this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                        /*else
                            startActivity(new Intent(InspectionsToCompleteActivity.this,PendingInspectionsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                */
                    }
                   /* else
                        Utilities.serverError(InspectionsToCompleteActivity.this);*/
                }
               /* else
                {
                    progressDialog.dismiss();
                    Utilities.serverError(InspectionsToCompleteActivity.this);
                }*/


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
