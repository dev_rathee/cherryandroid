package cherry.android.com.cherry;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import Utils.CherryAPI;
import Utils.Connectivity;
import Utils.Constants;
import Utils.GsonRequest;
import Utils.Utilities;
import beans.ContactInfoBean;

public class ContactInfoProfile extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Button cancelButton, saveButton,saveTextButton;
    TextView headerTextView;
    EditText firstNameEditText, lastNameEditText, emailIdEditText, zipEditText;
    Spinner genderSpinner, countrySpinner;
    String[] genderArray, countryArray;
    ArrayAdapter<String> genderArrayAdapter, countryArrayAdapter;
    String userId = "", selectedGender, selectedCountry;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info_profile);

        initialiseUI();
    }

    private void initialiseUI() {

        progressDialog = new ProgressDialog(ContactInfoProfile.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);

        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        emailIdEditText = (EditText) findViewById(R.id.emailIdEditText);
        zipEditText = (EditText) findViewById(R.id.zipcodeEditText);

        genderSpinner = (Spinner) findViewById(R.id.genderSpinner);
        genderArray = getResources().getStringArray(R.array.gender_array);
        genderArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, genderArray);
        genderArrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        genderSpinner.setAdapter(genderArrayAdapter);
        genderSpinner.setOnItemSelectedListener(this);

        countrySpinner = (Spinner) findViewById(R.id.countrySpinner);
        countryArray = getResources().getStringArray(R.array.country_array);
        countryArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, countryArray);
        countryArrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        countrySpinner.setAdapter(countryArrayAdapter);
        countrySpinner.setOnItemSelectedListener(this);

        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setVisibility(View.GONE);

        saveTextButton = (Button) findViewById(R.id.saveTextButton);
        saveTextButton.setOnClickListener(this);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText("Profile");

        if (Connectivity.isConnected(ContactInfoProfile.this))
            getUserInfo();

        else
            Utilities.internetConnectionError(ContactInfoProfile.this);
    }

    private void getUserInfo() {
        progressDialog.show();

        String Get_User_Info_Url = CherryAPI.ROOT_URL + CherryAPI.GET_POST_USER_INFO + "/" + LoginActivity.loginBean.getData().getUser().getId();

        GsonRequest<ContactInfoBean> contactInfoProfileGsonRequest = new GsonRequest<ContactInfoBean>(Request.Method.GET, Get_User_Info_Url, ContactInfoBean.class, null, new Response.Listener<ContactInfoBean>() {
            @Override
            public void onResponse(ContactInfoBean response) {

                progressDialog.dismiss();

                if (response.getStatus()) {
                    userId = response.getData().getId();
                    firstNameEditText.setText(response.getData().getFname());
                    lastNameEditText.setText(response.getData().getLname());
                    emailIdEditText.setText(response.getData().getEmail());
                    emailIdEditText.setEnabled(false);
                    zipEditText.setText(response.getData().getZipcode());

                    if (response.getData().getGender().equals("Male"))
                        genderSpinner.setSelection(0);

                    else if (response.getData().getGender().equals("Female"))
                        genderSpinner.setSelection(1);

                    if (response.getData().getCountry().equals("USA"))
                        countrySpinner.setSelection(0);

                    else if (response.getData().getCountry().equals("CANADA"))
                        countrySpinner.setSelection(1);

                    else if (response.getData().getCountry().equals("MEXICO"))
                        countrySpinner.setSelection(2);
                } else
                    Toast.makeText(ContactInfoProfile.this, "Data Unavailable.", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilities.serverError(ContactInfoProfile.this);
            }
        });

        contactInfoProfileGsonRequest.setShouldCache(false);
        Utilities.getRequestQueue(ContactInfoProfile.this).add(contactInfoProfileGsonRequest);

    }

    private boolean checkValidation() {

        if (firstNameEditText.getText().toString().trim().length() > 0) {
            if (lastNameEditText.getText().toString().trim().length() > 0) {
                if(zipEditText.getText().toString().trim().length() > 0) {
                    return true;
                }
                else
                {
                    Utilities.showToast(this, "Enter ZipCode");
                    return false;
                }
            } else {
                Utilities.showToast(this, "Enter Last Name");
                return false;
            }
        } else {
            Utilities.showToast(this, "Enter FirstName");
            return false;
        }
    }

    private void saveUserInfo() {
        if (Connectivity.isConnected(ContactInfoProfile.this)) {
            if (checkValidation()) {
                try {
                    String post_User_Info_Url = CherryAPI.ROOT_URL + CherryAPI.GET_POST_USER_INFO;

                    JSONObject postUserInfoObj = new JSONObject();
                    postUserInfoObj.put(Constants.USER_ID_CHANGE_PASSWORD, userId);
                    postUserInfoObj.put(Constants.USER_FIRST_NAME, firstNameEditText.getText().toString().trim());
                    postUserInfoObj.put(Constants.USER_LAST_NAME, lastNameEditText.getText().toString());
                    postUserInfoObj.put(Constants.USER_GENDER, selectedGender);
                    postUserInfoObj.put(Constants.USER_COUNTRY, selectedCountry);
                    postUserInfoObj.put(Constants.USER_ZIPCODE, zipEditText.getText().toString().trim());

                    new JsonObjectResponseAsync(ContactInfoProfile.this, postUserInfoObj).execute(post_User_Info_Url);

                } catch (Exception e) {

                }
            }

        } else
            Utilities.internetConnectionError(ContactInfoProfile.this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;

        if (spinner.getId() == R.id.genderSpinner)
            selectedGender = (String) parent.getItemAtPosition(position);

        if (spinner.getId() == R.id.countrySpinner)
            selectedCountry = (String) parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelButton:
               finish();
                break;

            case R.id.saveTextButton:
                saveUserInfo();

                break;
        }
    }

    public class JsonObjectResponseAsync extends AsyncTask<String, JSONObject, JSONObject> {
        Context mContext;
        JSONObject jsonObject;
        ProgressDialog progressDialog;

        public JsonObjectResponseAsync(Context context, JSONObject jsonObject) {
            this.mContext = context;
            this.jsonObject = jsonObject;
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading");
        }

        @Override
        protected JSONObject doInBackground(String[] urls) {

            JSONObject responseJsonObject = null;

            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 10000);
                HttpConnectionParams.setSoTimeout(myParams, 10000);

                HttpPost httpPost = new HttpPost(urls[0]);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                StringEntity se = new StringEntity(jsonObject.toString());
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String temp = EntityUtils.toString(httpResponse.getEntity());

                responseJsonObject = new JSONObject(temp);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseJsonObject;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }


        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            try {
                progressDialog.dismiss();
                if (!(response == null)) {
                    if (response.getBoolean("status")) {
                        Toast.makeText(ContactInfoProfile.this, "Customer Info Updated", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Utilities.serverError(ContactInfoProfile.this);
                        return;
                    }
                }
            } catch (Exception e) {
            }
        }


    }
}
