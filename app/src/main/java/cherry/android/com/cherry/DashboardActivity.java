package cherry.android.com.cherry;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

import Utils.Constants;


public class DashboardActivity extends ActionBarActivity implements View.OnClickListener{

    RelativeLayout newCustomerRelativeLayout,branchRelativeLayout,inspectionsRelativeLayout,settingsRelativeLayout;
    TextView branchTextView,onePersonTextView,twoPersonTextView,threePersonTextView,fourPersonTextView,fivePersonTextView;
    public static String selectedProcedure;
    public static String USER_NAME = "";
    private final String DESIGNATION_ADMIN = "Admin";
    private final String DESIGNATION_MEMBER = "Member";
    private final String DESIGNATION_OWNER = "Owner";
    boolean personSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initialiseUI();
    }

    private void initialiseUI() {

        USER_NAME = getRandomName();

        Constants.CURRENT_VIN = USER_NAME;

        newCustomerRelativeLayout = (RelativeLayout) findViewById(R.id.newCustomerRelativeLayout);

        branchRelativeLayout = (RelativeLayout) findViewById(R.id.branchRelativeLayout);
        branchRelativeLayout.setOnClickListener(this);

        inspectionsRelativeLayout = (RelativeLayout) findViewById(R.id.inspectionsRelativeLayout);
        inspectionsRelativeLayout.setOnClickListener(this);

        settingsRelativeLayout = (RelativeLayout) findViewById(R.id.settingsRelativeLayout);
        settingsRelativeLayout.setOnClickListener(this);

        if(!(LoginActivity.loginBean.getData().getUser().getDesignation().equals(DESIGNATION_ADMIN) || (LoginActivity.loginBean.getData().getUser().getDesignation().equals(DESIGNATION_OWNER))))
            settingsRelativeLayout.setVisibility(View.GONE);

        boolean status = LoginActivity.loginBean.getData().getSUBSCRIPTION_EXPIRED();

        if(status)
        {
            newCustomerRelativeLayout.setEnabled(false);
            branchRelativeLayout.setEnabled(false);
            inspectionsRelativeLayout.setEnabled(false);
        }

        branchTextView = (TextView) findViewById(R.id.branchTextView);
        if(!(LoginActivity.loginBean.getData() == null))
        branchTextView.setText(LoginActivity.loginBean.getData().getUserBranch().get(0).getName());


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(personSelected)
            startActivity(new Intent(DashboardActivity.this,DashboardActivity.class));

    }

    private String getRandomName() {
        String ALLOWED_CHARACTERS ="qwertyuiopasdfghjklzxcvbnm";

        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = 6;
        for (int i = 0; i < randomLength; i++){
            randomStringBuilder.append(ALLOWED_CHARACTERS.charAt(generator.nextInt(ALLOWED_CHARACTERS.length())));
        }
        return randomStringBuilder.toString();
    }

    public void accountSettingsClick(View view)
    {
        startActivity(new Intent(this, AccountSettingsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    public void closeAccountScreen(View view)
    {
        startActivity(new Intent(this,DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }



    public void newCustomerRelativeLayoutClick(View view)
    {
        setContentView(R.layout.layout_procedure_menu);

        personSelected = true;

        onePersonTextView = (TextView) findViewById(R.id.onePersonTextView);
        twoPersonTextView = (TextView) findViewById(R.id.twoPersonTextView);
        threePersonTextView = (TextView) findViewById(R.id.threePersonTextView);
        fourPersonTextView = (TextView) findViewById(R.id.fourPersonTextView);
        fivePersonTextView = (TextView) findViewById(R.id.fivePersonTextView);
        findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this,DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        onePersonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProcedure = "1PersonProcedure";
                startActivity(new Intent(DashboardActivity.this, InspectionsToCompleteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

            }
        });

        twoPersonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProcedure = "2PersonProcedure";
                startActivity(new Intent(DashboardActivity.this, InspectionsToCompleteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

            }
        });

        threePersonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProcedure = "3PersonProcedure";
                startActivity(new Intent(DashboardActivity.this, InspectionsToCompleteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

            }
        });

        fourPersonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProcedure = "4PersonProcedure";
                startActivity(new Intent(DashboardActivity.this, InspectionsToCompleteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(Constants.INSPECTIONS_LIST_FROM,Constants.FROM_CUSTOMER_INFO));

            }
        });

        fivePersonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProcedure = "5PersonProcedure";
                startActivity(new Intent(DashboardActivity.this, InspectionsToCompleteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(Constants.INSPECTIONS_LIST_FROM,Constants.FROM_CUSTOMER_INFO));

            }
        });
    }


    public void commonLogout(View v)
    {
        startActivity(new Intent(this,LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onClick(View v) {


       if(v.getId() == R.id.branchRelativeLayout)
        {
           //  startActivity(new Intent(DashboardActivity.this,CustomerInfoActivity.class));
        }
        else if(v.getId() == R.id.inspectionsRelativeLayout)
        {
          startActivity(new Intent(DashboardActivity.this,PendingInspectionsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }

        else if(v.getId() == R.id.settingsRelativeLayout)
        {
            setContentView(R.layout.layout_account_setting_menu);

           // startActivity(new Intent(DashboardActivity.this,AccountSettingsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }


    }
}
