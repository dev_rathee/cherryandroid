package cherry.android.com.cherry;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import Utils.CherryAPI;
import Utils.Connectivity;
import Utils.Constants;
import Utils.Utilities;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    EditText firstNameEditText,lastNameEditText,emailEditText,passwordEditText;
    TextView signInTextView;
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initialiseUI();
    }

    private void initialiseUI() {

        firstNameEditText = (EditText) findViewById(R.id.signup_firstNameEditText);
        lastNameEditText = (EditText) findViewById(R.id.signup_lastNameEditText);
        emailEditText = (EditText) findViewById(R.id.signup_emailEditText);
        passwordEditText = (EditText) findViewById(R.id.signup_passwordEditText);

        signInTextView = (TextView) findViewById(R.id.loginTextView);
        signInTextView.setOnClickListener(this);

        signUpButton = (Button) findViewById(R.id.signUpButton);
        signUpButton.setOnClickListener(this);
    }

    private void signUp()
    {
        if(Connectivity.isConnected(SignUpActivity.this)) {

            try {


                String url = CherryAPI.ROOT_URL + CherryAPI.SIGNUP_URL;

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.LOGIN_EMAIL_ID, emailEditText.getText().toString().trim());
                jsonObject.put(Constants.USER_FIRST_NAME, firstNameEditText.getText().toString().trim());
                jsonObject.put(Constants.USER_LAST_NAME, lastNameEditText.getText().toString());
                jsonObject.put(Constants.LOGIN_PASSWORD, passwordEditText.getText().toString().trim());

                new JsonObjectResponseAsync(SignUpActivity.this,jsonObject).execute(url);

            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }
        else
            Utilities.internetConnectionError(SignUpActivity.this);

    }

    private boolean checkValidation()
    {
        if(emailEditText.getText().toString().trim().length() > 0)
        {
             if(passwordEditText.getText().toString().trim().length() > 0)
             {
                 if(firstNameEditText.getText().toString().trim().length() > 0)
                 {
                     if(lastNameEditText.getText().toString().trim().length() > 0)
                     return true;

                     else
                     {
                         Utilities.showToast(SignUpActivity.this,"Enter Last Name");
                         return false;
                     }
                 }
                 else
                 {
                     Utilities.showToast(SignUpActivity.this,"Enter First Name");
                     return false;
                 }
             }
            else
             {
                 Utilities.showToast(SignUpActivity.this,"Enter Password");
                 return false;
             }

        }
        else
        {
            Utilities.showToast(SignUpActivity.this,"Enter Email");
            return false;
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signUpButton :

                if(checkValidation())
                 signUp();

                break;

            case R.id.loginTextView :
                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
                break;

        }
    }

    public class JsonObjectResponseAsync extends AsyncTask<String, JSONObject, JSONObject> {
        Context mContext;
        JSONObject jsonObject;
        ProgressDialog progressDialog;

        public JsonObjectResponseAsync(Context context, JSONObject jsonObject) {
            this.mContext = context;
            this.jsonObject = jsonObject;
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading");
        }

        @Override
        protected JSONObject doInBackground(String[] urls) {

            JSONObject responseJsonObject = null;

            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 10000);
                HttpConnectionParams.setSoTimeout(myParams, 10000);

                HttpPost httpPost = new HttpPost(urls[0]);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                StringEntity se = new StringEntity(jsonObject.toString());
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String temp = EntityUtils.toString(httpResponse.getEntity());

                responseJsonObject = new JSONObject(temp);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseJsonObject;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }


        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            try {
                progressDialog.dismiss();
                if (!(response == null)) {
                    if (response.getBoolean("status")) {
                        Toast.makeText(SignUpActivity.this, "SignUp Successful.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Utilities.serverError(SignUpActivity.this);
                        return;
                    }
                }
            } catch (Exception e) {
            }
        }


    }
}
