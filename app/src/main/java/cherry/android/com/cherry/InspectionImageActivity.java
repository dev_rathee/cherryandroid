package cherry.android.com.cherry;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class InspectionImageActivity extends AppCompatActivity {


    private final int CAMERA_REQUEST = 125, SELECT_IMAGE = 126;
    public static String selectedImage = "";
    ImageView inspectionImageView;
    public static Bitmap selectedBitmap;

    /*private Bitmap getBitmapFromBase64(String base64)
    {
        byte[] imageAsBytes = Base64.decode(base64. getBytes(), Base64. DEFAULT);
        return BitmapFactory. decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_image_inspection);

        inspectionImageView = (ImageView) findViewById(R.id.inspectionImageView);

        if(!(selectedBitmap == null))
            inspectionImageView.setImageBitmap(selectedBitmap);


        findViewById(R.id.takePhotoTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto();
            }
        });

        findViewById(R.id.cancelTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedImage = "";
                selectedBitmap = null;
                finish();
            }
        });

        findViewById(R.id.backImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.saveTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.selectPhotoImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPhoto();
            }
        });
    }

    private void convertBitmapToBase64(Bitmap bitmap) {
        bitmap = Bitmap.createScaledBitmap(bitmap,300,300,true);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        selectedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        selectedBitmap = bitmap;
        inspectionImageView.setImageBitmap(selectedBitmap);
    }

    @Override
    protected void onActivityResult(final int requestCode,final int resultCode,final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select option for Selected Image");
        builder.setItems(new CharSequence[]
                        {"Choose", "Cancel"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                findViewById(R.id.cancelTextView).setVisibility(View.VISIBLE);
                                findViewById(R.id.saveTextView).setVisibility(View.VISIBLE);
                                findViewById(R.id.backImageView).setVisibility(View.GONE);

                                if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
                                    Bitmap photoBitmap = (Bitmap) data.getExtras().get("data");
                                    convertBitmapToBase64(photoBitmap);
                                }

                                if(requestCode == SELECT_IMAGE && resultCode == RESULT_OK)
                                {
                                    try {
                                        Uri selectedImage = data.getData();
                                        InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                                        Bitmap yourSelectedImageBitmap = BitmapFactory.decodeStream(imageStream);
                                        convertBitmapToBase64(yourSelectedImageBitmap);
                                    }catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                                break;
                            case 1:
                                break;


                        }
                    }
                });
        builder.create().show();


    }


    private void takePhoto() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectPhoto() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_IMAGE);
        } catch (Exception e) {

        }
    }

}
