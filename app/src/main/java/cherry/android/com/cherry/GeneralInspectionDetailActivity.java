package cherry.android.com.cherry;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import Adapters.GeneralInspectionsExpandableListAdapter;
import Utils.Constants;
import Utils.Utilities;
import beans.SelectedInspection;


public class GeneralInspectionDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private final int CAMERA_REQUEST = 125, SELECT_IMAGE = 126;
    TextView technicianNameTextView, saveTextView, twoQuantityTextView, threeQuantityTextView, customQuantityTextView, oneDefaultCommentTextView, twoDefaultCommentTextView, customDefaultCommentTextView, headerTextView, saveGeneralInspectionDetailsTextView;
    boolean oneDefaultCommentTextViewSelected, twoDefaultCommentTextViewSelected, customDefaultCommentTextViewSelected,
            twoQuantityTextViewSelected, threeQuantityTextViewSelected, customQuantityTextViewSelected,
            checkedCircularImageViewClicked, recordCircularImageViewClicked, naCircularImageViewClicked, inspectionSelected;
    RelativeLayout inspectionChildView;
    ImageView recordCircularImageView, checkedImageView, naImageView, menuCircularImageView;
    EditText customQuantityEditText, customCommentEditText;
    String selectedQuantity = "", selectedComment = "" /*,imageBase64="", selectedImage = ""*/;
    SelectedInspection selectedInspection;
    Button cancelButton, saveButton, cameraButton;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_inspection_detail);

        hideSoftKeyboard();
        initialiseUI();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void initialiseUI() {

        InspectionImageActivity.selectedImage = "";

        inspectionChildView = (RelativeLayout) findViewById(R.id.inspectionChildView);

        saveGeneralInspectionDetailsTextView = (TextView) findViewById(R.id.saveGeneralInspectionDetailsTextView);
        saveGeneralInspectionDetailsTextView.setOnClickListener(this);

        customQuantityEditText = (EditText) findViewById(R.id.customQuantityEditText);
        customCommentEditText = (EditText) findViewById(R.id.customCommentEditText);

        naImageView = (ImageView) inspectionChildView.findViewById(R.id.naImageView);
        naImageView.setOnClickListener(this);

        menuCircularImageView = (ImageView) inspectionChildView.findViewById(R.id.menuCircularImageView);
        menuCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        menuCircularImageView.setBackgroundColor(getResources().getColor(R.color.app_pink));
        menuCircularImageView.setOnClickListener(this);

        checkedImageView = (ImageView) inspectionChildView.findViewById(R.id.chechedCircularImageView);
        checkedImageView.setOnClickListener(this);

        recordCircularImageView = (ImageView) inspectionChildView.findViewById(R.id.recordCircularImageView);
        //recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_selected));
        //recordCircularImageViewClicked = true;
        recordCircularImageView.setOnClickListener(this);

        String imagePath = GeneralInspectionsExpandableListAdapter.selectedService.getImage_path();

        TextView inspectionRowTextView = (TextView) inspectionChildView.findViewById(R.id.inspection_TextView_Row);
        inspectionRowTextView.setText(imagePath);

        if (imagePath.contains("-"))
            imagePath = imagePath.replace("-", "_");

        imagePath = imagePath.toLowerCase();

        int resourceId = getResources().getIdentifier(imagePath, "drawable", getPackageName());
        if (!(resourceId == 0)) {
            Drawable drawable = getResources().getDrawable(resourceId);

            ImageView inspectionImageView = (ImageView) inspectionChildView.findViewById(R.id.inspectionImageView);
            inspectionImageView.setImageDrawable(drawable);
        }

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText(DashboardActivity.USER_NAME);

        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setText("");
        saveButton.setOnClickListener(this);

        cameraButton = (Button) findViewById(R.id.cameraButton);
        cameraButton.setOnClickListener(this);

        technicianNameTextView = (TextView) findViewById(R.id.technicianNameTextView);
        technicianNameTextView.setText(GeneralInspectionsExpandableListAdapter.technicianName);

        saveTextView = (TextView) findViewById(R.id.saveGeneralInspectionDetailsTextView);
        saveTextView.setOnClickListener(this);

        twoQuantityTextView = (TextView) findViewById(R.id.twoQuantityTextView);
        twoQuantityTextView.setOnClickListener(this);

        threeQuantityTextView = (TextView) findViewById(R.id.threeQuantityTextView);
        threeQuantityTextView.setOnClickListener(this);

        customQuantityTextView = (TextView) findViewById(R.id.customQuantityTextView);
        customQuantityTextView.setOnClickListener(this);

        oneDefaultCommentTextView = (TextView) findViewById(R.id.oneDefaultCommentTextView);
        oneDefaultCommentTextView.setText(GeneralInspectionsExpandableListAdapter.selectedService.getDefault_comment1());
        oneDefaultCommentTextView.setOnClickListener(this);

        twoDefaultCommentTextView = (TextView) findViewById(R.id.twoDefaultCommentTextView);
        twoDefaultCommentTextView.setText(GeneralInspectionsExpandableListAdapter.selectedService.getDefault_comment2());
        twoDefaultCommentTextView.setOnClickListener(this);

        customDefaultCommentTextView = (TextView) findViewById(R.id.customCommentTextView);
        customDefaultCommentTextView.setOnClickListener(this);


        for (int i = 0; i < Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN).size(); i++) {

            if (Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN).get(i).getId().equals(GeneralInspectionsExpandableListAdapter.selectedService.getId())) {

                String quantity = Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN).get(i).getQuantity();
                if (quantity.equals("3")) {
                    recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_selected));
                    recordCircularImageViewClicked = true;
                    threeQuantityTextViewSelected = true;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_white));
                } else if (quantity.equals("2")) {
                    recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_selected));
                    recordCircularImageViewClicked = true;
                    twoQuantityTextViewSelected = true;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_white));
                } else if (quantity.equals("0")) {
                    naCircularImageViewClicked = true;
                    naImageView.setImageDrawable(getResources().getDrawable(R.drawable.na_selected));

                    cameraButton.setEnabled(true);

                    threeQuantityTextViewSelected = false;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    threeQuantityTextView.setEnabled(false);

                    twoQuantityTextViewSelected = false;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    twoQuantityTextView.setEnabled(false);

                    customQuantityEditText.setVisibility(View.GONE);

                    customQuantityTextView.setText("Custom Quantity");
                    customQuantityTextViewSelected = false;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    customQuantityTextView.setEnabled(false);

                } else if (quantity.equals("-1")) {
                    checkedImageView.setImageDrawable(getResources().getDrawable(R.drawable.checked_selected));
                    checkedCircularImageViewClicked = true;

                    cameraButton.setEnabled(true);

                    oneDefaultCommentTextViewSelected = false;
                    oneDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    oneDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                    oneDefaultCommentTextView.setEnabled(false);

                    twoDefaultCommentTextViewSelected = false;
                    twoDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                    twoDefaultCommentTextView.setEnabled(false);

                    customCommentEditText.setVisibility(View.GONE);
                    customDefaultCommentTextView.setText("Custom Comment");
                    customDefaultCommentTextViewSelected = false;
                    customDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                    customDefaultCommentTextView.setEnabled(false);

                    threeQuantityTextViewSelected = false;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    threeQuantityTextView.setEnabled(false);

                    twoQuantityTextViewSelected = false;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    twoQuantityTextView.setEnabled(false);

                    customQuantityEditText.setVisibility(View.GONE);

                    customQuantityTextView.setText("Custom Quantity");
                    customQuantityTextViewSelected = false;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    customQuantityTextView.setEnabled(false);
                } else {
                    recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_selected));
                    recordCircularImageViewClicked = true;

                    customQuantityEditText.setVisibility(View.VISIBLE);
                    customQuantityTextViewSelected = true;

                    if (quantity.equals("")) {
                        customQuantityTextView.setText("");
                        customQuantityEditText.setText("1");
                    } else {
                        customQuantityTextView.setText("");
                        customQuantityEditText.setText(quantity);
                    }

                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    customQuantityEditText.setTextColor(getResources().getColor(R.color.app_white));

                }

                String comment = Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN).get(i).getComment();

                if (comment.equals(GeneralInspectionsExpandableListAdapter.selectedService.getDefault_comment1())) {
                    oneDefaultCommentTextViewSelected = true;
                    oneDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    oneDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_white));

                } else if (comment.equals(GeneralInspectionsExpandableListAdapter.selectedService.getDefault_comment2())) {
                    twoDefaultCommentTextViewSelected = true;
                    twoDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    twoDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_white));

                } else {
                    if (!(comment.equals(""))) {
                        customCommentEditText.setVisibility(View.VISIBLE);

                        customDefaultCommentTextViewSelected = true;
                        customCommentEditText.setText(comment);

                        customDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                        customCommentEditText.setTextColor(getResources().getColor(R.color.app_white));

                    }
                }

                inspectionSelected = true;
                bitmap = Utilities.getSelectedInspectionArrayList(Constants.CURRENT_VIN).get(i).getBitmap();

                if (!(bitmap == null)) {
                    InspectionImageActivity.selectedBitmap = bitmap;
                    (findViewById(R.id.checkedUncheckedImageView)).setBackgroundDrawable(getResources().getDrawable(R.drawable.checked_selected));
                }
                else
                    InspectionImageActivity.selectedBitmap = null;

            }
        }

        if (!inspectionSelected) {
            InspectionImageActivity.selectedBitmap = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!(InspectionImageActivity.selectedImage.equals("")))
            (findViewById(R.id.checkedUncheckedImageView)).setBackgroundDrawable(getResources().getDrawable(R.drawable.checked_selected));


    }

    private boolean getSelectedQuantity() {
        if (twoQuantityTextViewSelected) {
            selectedQuantity = "2";
            return true;
        } else if (threeQuantityTextViewSelected) {
            selectedQuantity = "3";
            return true;
        } else if (customQuantityTextViewSelected) {

            if (customQuantityEditText.getText().toString().trim().length() > 0) {
                selectedQuantity = customQuantityEditText.getText().toString().trim();
                return true;
            } else {
                selectedQuantity = "1";
                return true;
            }
        } else {
            selectedQuantity = "1";
            return true;
        }
    }

    private boolean getSelectedComment() {
        if (twoDefaultCommentTextViewSelected) {
            selectedComment = twoDefaultCommentTextView.getText().toString();
            return true;
        } else if (oneDefaultCommentTextViewSelected) {
            selectedComment = oneDefaultCommentTextView.getText().toString();
            return true;
        } else if (customDefaultCommentTextViewSelected) {

            if (customCommentEditText.getText().toString().trim().length() > 0) {
                selectedComment = customCommentEditText.getText().toString().trim();
                return true;
            } else {
                selectedComment = "";
                // Toast.makeText(GeneralInspectionDetailActivity.this, "Enter Comment.", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            selectedComment = "";
            // Toast.makeText(GeneralInspectionDetailActivity.this, "Select Comment.", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

   /* private void takePhoto() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectPhoto() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image*//*");
            startActivityForResult(intent, SELECT_IMAGE);
        } catch (Exception e) {

        }
    }

    private void showImageOptions() {



        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select one option from below");
        builder.setItems(new CharSequence[]
                        {"Take Photo", "Select Photo", "Cancel"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                takePhoto();
                                break;
                            case 1:
                                selectPhoto();
                                break;


                        }
                    }
                });
        builder.create().show();

    }*/


  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap photoBitmap = (Bitmap) data.getExtras().get("data");
            convertBitmapToBase64(photoBitmap);
        }

        if(requestCode == SELECT_IMAGE && resultCode == RESULT_OK)
        {
            try {
                Uri selectedImage = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }*/

    /*private void convertBitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        selectedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        Log.i("Selected Image : ", selectedImage);
    }*/

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.saveButton:
                startActivity(new Intent(GeneralInspectionDetailActivity.this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;

            case R.id.cancelButton:
                finish();
                break;

            case R.id.menuCircularImageView:

                finish();
                break;

            case R.id.saveGeneralInspectionDetailsTextView:

                if (recordCircularImageViewClicked) {
                    if (getSelectedQuantity()) {
                        if (getSelectedComment()) {
                            selectedInspection = new SelectedInspection("", "", GeneralInspectionsExpandableListAdapter.selectedService.getId(), selectedComment, selectedQuantity, InspectionImageActivity.selectedImage, InspectionImageActivity.selectedBitmap);
                            Utilities.updateInspection(selectedInspection);
                            finish();
                        }
                    }
                } else if (checkedCircularImageViewClicked) {
                    selectedInspection = new SelectedInspection("", "", GeneralInspectionsExpandableListAdapter.selectedService.getId(), "", "-1", "", InspectionImageActivity.selectedBitmap);
                    Utilities.updateInspection(selectedInspection);
                    finish();
                } else if (naCircularImageViewClicked) {
                    if (getSelectedComment()) {
                        selectedInspection = new SelectedInspection("", "", GeneralInspectionsExpandableListAdapter.selectedService.getId(), selectedComment, "0", InspectionImageActivity.selectedImage, InspectionImageActivity.selectedBitmap);
                        Utilities.updateInspection(selectedInspection);
                        finish();
                    }
                }

                break;

            case R.id.recordCircularImageView:

                if (recordCircularImageViewClicked) {
                    recordCircularImageViewClicked = false;
                    recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_unselected));
                } else {
                    recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_selected));

                    recordCircularImageViewClicked = true;
                    cameraButton.setEnabled(true);
                    oneDefaultCommentTextView.setEnabled(true);
                    twoDefaultCommentTextView.setEnabled(true);
                    customDefaultCommentTextView.setEnabled(true);
                    threeQuantityTextView.setEnabled(true);
                    twoQuantityTextView.setEnabled(true);
                    customQuantityTextView.setEnabled(true);

                    checkedImageView.setImageDrawable(getResources().getDrawable(R.drawable.checked_unselected));
                    checkedCircularImageViewClicked = false;

                    naCircularImageViewClicked = false;
                    naImageView.setImageDrawable(getResources().getDrawable(R.drawable.na_unselected));


                    //  GeneralInspectionsExpandableListAdapter.checkedServicesMap.remove(GeneralInspectionsExpandableListAdapter.selectedService);

                }

                break;

            case R.id.chechedCircularImageView:
                if (checkedCircularImageViewClicked) {
                    checkedImageView.setImageDrawable(getResources().getDrawable(R.drawable.checked_unselected));
                    checkedCircularImageViewClicked = false;

                    oneDefaultCommentTextView.setEnabled(true);
                    twoDefaultCommentTextView.setEnabled(true);
                    customDefaultCommentTextView.setEnabled(true);
                    threeQuantityTextView.setEnabled(true);
                    twoQuantityTextView.setEnabled(true);
                    customQuantityTextView.setEnabled(true);


                } else {
                    checkedImageView.setImageDrawable(getResources().getDrawable(R.drawable.checked_selected));
                    checkedCircularImageViewClicked = true;

                   // cameraButton.setEnabled(false);

                    recordCircularImageViewClicked = false;
                    recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_unselected));

                    naCircularImageViewClicked = false;
                    naImageView.setImageDrawable(getResources().getDrawable(R.drawable.na_unselected));

                    oneDefaultCommentTextViewSelected = false;
                    oneDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    oneDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                    oneDefaultCommentTextView.setEnabled(false);

                    twoDefaultCommentTextViewSelected = false;
                    twoDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                    twoDefaultCommentTextView.setEnabled(false);

                    customCommentEditText.setVisibility(View.GONE);
                    customDefaultCommentTextView.setText("Custom Comment");
                    customDefaultCommentTextViewSelected = false;
                    customDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                    customDefaultCommentTextView.setEnabled(false);

                    threeQuantityTextViewSelected = false;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    threeQuantityTextView.setEnabled(false);

                    twoQuantityTextViewSelected = false;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    twoQuantityTextView.setEnabled(false);

                    customQuantityEditText.setVisibility(View.GONE);

                    customQuantityTextView.setText("Custom Quantity");
                    customQuantityTextViewSelected = false;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    customQuantityTextView.setEnabled(false);

                }

                break;

            case R.id.naImageView:

                if (naCircularImageViewClicked) {
                    naCircularImageViewClicked = false;
                    naImageView.setImageDrawable(getResources().getDrawable(R.drawable.na_unselected));

                    threeQuantityTextView.setEnabled(true);
                    twoQuantityTextView.setEnabled(true);
                    customQuantityTextView.setEnabled(true);
                } else {
                    naCircularImageViewClicked = true;
                    naImageView.setImageDrawable(getResources().getDrawable(R.drawable.na_selected));

                    cameraButton.setEnabled(true);
                    oneDefaultCommentTextView.setEnabled(true);
                    twoDefaultCommentTextView.setEnabled(true);
                    customDefaultCommentTextView.setEnabled(true);

                    threeQuantityTextViewSelected = false;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    threeQuantityTextView.setEnabled(false);

                    twoQuantityTextViewSelected = false;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    twoQuantityTextView.setEnabled(false);

                    customQuantityEditText.setVisibility(View.GONE);

                    customQuantityTextView.setText("Custom Quantity");
                    customQuantityTextViewSelected = false;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                    customQuantityTextView.setEnabled(false);

                    recordCircularImageViewClicked = false;
                    recordCircularImageView.setImageDrawable(getResources().getDrawable(R.drawable.record_unselected));

                    checkedImageView.setImageDrawable(getResources().getDrawable(R.drawable.checked_unselected));
                    checkedCircularImageViewClicked = false;


                }

                break;

            case R.id.oneDefaultCommentTextView:
                if (oneDefaultCommentTextViewSelected) {
                    oneDefaultCommentTextViewSelected = false;
                    oneDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    oneDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                } else {
                    oneDefaultCommentTextViewSelected = true;
                    oneDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    oneDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_white));

                    twoDefaultCommentTextViewSelected = false;
                    twoDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));

                    customCommentEditText.setVisibility(View.GONE);
                    customDefaultCommentTextView.setText("Custom Comment");
                    customDefaultCommentTextViewSelected = false;
                    customDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));

                }


                break;

            case R.id.twoDefaultCommentTextView:
                if (twoDefaultCommentTextViewSelected) {
                    twoDefaultCommentTextViewSelected = false;
                    twoDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                } else {
                    twoDefaultCommentTextViewSelected = true;
                    twoDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    twoDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_white));

                    oneDefaultCommentTextViewSelected = false;
                    oneDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    oneDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));

                    customCommentEditText.setVisibility(View.GONE);
                    customDefaultCommentTextView.setText("Custom Comment");
                    customDefaultCommentTextViewSelected = false;
                    customDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));

                }
                break;

            case R.id.customCommentTextView:
                if (customDefaultCommentTextViewSelected) {

                    customCommentEditText.setVisibility(View.GONE);
                    customDefaultCommentTextView.setText("Custom Comment");

                    customDefaultCommentTextViewSelected = false;
                    customDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));
                } else {

                    customCommentEditText.setVisibility(View.VISIBLE);
                    customDefaultCommentTextView.setText("");

                    customDefaultCommentTextViewSelected = true;
                    customDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    customDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_white));

                    oneDefaultCommentTextViewSelected = false;
                    oneDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    oneDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));

                    twoDefaultCommentTextViewSelected = false;
                    twoDefaultCommentTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoDefaultCommentTextView.setTextColor(getResources().getColor(R.color.app_black));

                }
                break;

            case R.id.twoQuantityTextView:
                if (twoQuantityTextViewSelected) {
                    twoQuantityTextViewSelected = false;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                } else {
                    twoQuantityTextViewSelected = true;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_white));

                    threeQuantityTextViewSelected = false;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));

                    customQuantityEditText.setVisibility(View.GONE);
                    customQuantityTextView.setText("Custom Quantity");
                    customQuantityTextViewSelected = false;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));

                }
                break;

            case R.id.threeQuantityTextView:
                if (threeQuantityTextViewSelected) {
                    threeQuantityTextViewSelected = false;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                } else {
                    threeQuantityTextViewSelected = true;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_white));

                    twoQuantityTextViewSelected = false;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));

                    customQuantityEditText.setVisibility(View.GONE);
                    customQuantityTextView.setText("Custom Quantity");
                    customQuantityTextViewSelected = false;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));

                }
                break;

            case R.id.customQuantityTextView:
                if (customQuantityTextViewSelected) {
                    customQuantityEditText.setVisibility(View.GONE);
                    customQuantityTextView.setText("Custom Quantity");
                    customQuantityTextViewSelected = false;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));
                } else {


                    customQuantityEditText.setVisibility(View.VISIBLE);
                    customQuantityTextView.setText("");

                    customQuantityTextViewSelected = true;
                    customQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_pink));
                    customQuantityTextView.setTextColor(getResources().getColor(R.color.app_white));

                    twoQuantityTextViewSelected = false;
                    twoQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    twoQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));

                    threeQuantityTextViewSelected = false;
                    threeQuantityTextView.setBackgroundColor(getResources().getColor(R.color.app_white));
                    threeQuantityTextView.setTextColor(getResources().getColor(R.color.app_black));

                }
                break;

            case R.id.cameraButton:
                startActivity(new Intent(GeneralInspectionDetailActivity.this, InspectionImageActivity.class));
                break;
        }

    }
}
