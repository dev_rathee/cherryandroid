package cherry.android.com.cherry;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

import Utils.CherryAPI;
import Utils.Connectivity;
import Utils.Constants;
import Utils.GsonRequest;
import Utils.Utilities;
import beans.SearchVinBean;


public class CustomerInfoActivity extends ActionBarActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    TextView headerTextView;
    EditText customerNameEditText, customerLicenseEditText, customerVINEditText,
            customerYearEditText, customerMakeIdEditText, customerModelIdEditText, customerMileageEditText;
    Button cancelCustomerButton, saveCustomerButton;
    Spinner customerProcedureSpinner, customerCountrySpinner, customerStateSpinner;
    String[] procedureSpinnerArray, countrySpinnerArray, stateSpinnerArray;
    ArrayAdapter<String> procedureAdapter, countryAdapter, stateAdapter;
    public static String selectedProcedure = "1 Person Procedure", customerName = "";
    String selectedCountry = "",selectedState = "";
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_info);

        initialiseUI();

    }

    private void initialiseUI() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);

        procedureSpinnerArray = getResources().getStringArray(R.array.procedure_array);
        countrySpinnerArray = getResources().getStringArray(R.array.country_array);
        stateSpinnerArray = getResources().getStringArray(R.array.usa_states_array);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText("Inspections To Complete");

        cancelCustomerButton = (Button) findViewById(R.id.cancelButton);
        cancelCustomerButton.setVisibility(View.GONE);
        cancelCustomerButton.setEnabled(false);
        cancelCustomerButton.setClickable(false);


        saveCustomerButton = (Button) findViewById(R.id.saveButton);
        saveCustomerButton.setText("Save");
        saveCustomerButton.setBackgroundDrawable(null);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMargins(0, 0, 20, 0);
        saveCustomerButton.setLayoutParams(params);
        saveCustomerButton.setOnClickListener(this);

        customerNameEditText = (EditText) findViewById(R.id.customerNameEditText);

        customerLicenseEditText = (EditText) findViewById(R.id.customerLicenseEditText);
        customerVINEditText = (EditText) findViewById(R.id.customerVINEditText);
        customerYearEditText = (EditText) findViewById(R.id.customerYearEditText);
        customerMakeIdEditText = (EditText) findViewById(R.id.customerMakeIdEditText);
        customerModelIdEditText = (EditText) findViewById(R.id.customerModelIdEditText);
        customerMileageEditText = (EditText) findViewById(R.id.customerMileageEditText);

        customerStateSpinner = (Spinner) findViewById(R.id.customerStateProvinceSpinner);
        customerProcedureSpinner = (Spinner) findViewById(R.id.customerProcedureSpinner);
        customerCountrySpinner = (Spinner) findViewById(R.id.customerCountrySpinner);

        procedureAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, procedureSpinnerArray);
        procedureAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        customerProcedureSpinner.setAdapter(procedureAdapter);
        customerProcedureSpinner.setOnItemSelectedListener(this);

        stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateSpinnerArray);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        customerStateSpinner.setAdapter(stateAdapter);
        customerStateSpinner.setOnItemSelectedListener(this);

        // if(DashboardActivity.selectedProcedure.equals(procedureSpinnerArray[0].trim()))

        countryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countrySpinnerArray);
        countryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        customerCountrySpinner.setAdapter(countryAdapter);
        customerCountrySpinner.setOnItemSelectedListener(this);

        setSpinnerSelection(DashboardActivity.selectedProcedure);

        setValues();
    }

    private void setSpinnerSelection(String procedure)
    {
        if(procedure.equals("1PersonProcedure"))
            customerProcedureSpinner.setSelection(0);

        else if(procedure.equals("2PersonProcedure"))
            customerProcedureSpinner.setSelection(1);

        else if(procedure.equals("3PersonProcedure"))
            customerProcedureSpinner.setSelection(2);

        else if(procedure.equals("4PersonProcedure"))
            customerProcedureSpinner.setSelection(3);

        else
            customerProcedureSpinner.setSelection(4);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!(progressDialog == null)) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    private void enableAll() {
        customerNameEditText.setEnabled(true);
        customerCountrySpinner.setEnabled(true);
        customerStateSpinner.setEnabled(true);
        customerLicenseEditText.setEnabled(true);
        customerVINEditText.setEnabled(true);
        customerYearEditText.setEnabled(true);
        customerMakeIdEditText.setEnabled(true);
        customerModelIdEditText.setEnabled(true);
    }

    private void setValues() {

        enableAll();

        if (!(Utilities.getCustomerInfo(Constants.CURRENT_VIN) == null) && !Utilities.getCustomerInfo(Constants.CURRENT_VIN).getYear().equals("XXX")) {
            customerName = Utilities.getCustomerInfo(Constants.CURRENT_VIN).getName();
            customerNameEditText.setText(customerName);
            // customerStateProvinceEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getState());
            customerLicenseEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getLicence());
            customerVINEditText.setText(Constants.CURRENT_VIN);
            customerYearEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getYear());
            customerMakeIdEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getMake_id());
            customerModelIdEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getModel_id());
            customerMileageEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getMileage());
            if(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getCreated_at() == null)
              setSpinnerSelection("1PersonProcedure");

            else
            setSpinnerSelection(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getCreated_at());
        }


     /*   if (Constants.CUSTOMER_INFO_FROM.equals(Constants.FROM_SCAN_VIN)) {

            if(!(ScanVINActivity.searchVinBean.getData() == null) && ScanVINActivity.searchVinBean.getData().size() > 0) {
                customerName = ScanVINActivity.searchVinBean.getData().get(0).getName();
                customerNameEditText.setText(customerName);
                customerStateProvinceEditText.setText(ScanVINActivity.searchVinBean.getData().get(0).getState());
                customerLicenseEditText.setText(ScanVINActivity.searchVinBean.getData().get(0).getLicence());
                customerVINEditText.setText(ScanVINActivity.searchVinBean.getData().get(0).getVin());
                customerYearEditText.setText(ScanVINActivity.searchVinBean.getData().get(0).getYear());
                customerMakeIdEditText.setText(ScanVINActivity.searchVinBean.getData().get(0).getMake_id());
                customerModelIdEditText.setText(ScanVINActivity.searchVinBean.getData().get(0).getModel_id());
                customerMileageEditText.setText(ScanVINActivity.searchVinBean.getData().get(0).getMileage());
            }
        } else {
            customerName = PendingInspectionsActivity.searchBeanPendingInspection.getName();
            customerNameEditText.setText(customerName);
            customerStateProvinceEditText.setText(PendingInspectionsActivity.searchBeanPendingInspection.getState());
            customerLicenseEditText.setText(PendingInspectionsActivity.searchBeanPendingInspection.getLicence());
            customerVINEditText.setText(PendingInspectionsActivity.searchBeanPendingInspection.getVin());
            customerYearEditText.setText(PendingInspectionsActivity.searchBeanPendingInspection.getYear());
            customerMakeIdEditText.setText(PendingInspectionsActivity.searchBeanPendingInspection.getMake_id());
            customerModelIdEditText.setText(PendingInspectionsActivity.searchBeanPendingInspection.getModel_id());
            customerMileageEditText.setText(PendingInspectionsActivity.searchBeanPendingInspection.getMileage());
        }
*/
           /* customerCountrySpinner.setEnabled(false);
            customerStateProvinceEditText.setEnabled(false);
            customerLicenseEditText.setEnabled(false);*/
        customerVINEditText.setEnabled(false);
           /* customerYearEditText.setEnabled(false);
            customerMakeIdEditText.setEnabled(false);
            customerNameEditText.setEnabled(false);
            customerModelIdEditText.setEnabled(false);
            customerMileageEditText.setEnabled(false);*/


        if (!(Constants.CURRENT_VIN.length() == 17))
            (findViewById(R.id.customerVINLinearLayout)).setVisibility(View.GONE);

        else
            customerVINEditText.setEnabled(false);



        /*if(Constants.CURRENT_VIN.length() == 17 && !(Utilities.getCustomerInfo(Constants.CURRENT_VIN) == null))
        {
            customerName = PendingInspectionsActivity.searchBeanPendingInspection.getName();
            customerNameEditText.setText(customerName);
            customerStateProvinceEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getState());
            customerLicenseEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getLicence());
            customerVINEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getVin());
            customerYearEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getYear());
            customerMakeIdEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getMake_id());
            customerModelIdEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getModel_id());
            customerMileageEditText.setText(Utilities.getCustomerInfo(Constants.CURRENT_VIN).getMileage());

        }*/

    }

    private String getProcedure(String procedure)
    {
        if(procedure.equals("1 Person Procedure"))
            return "1PersonProcedure";

        else if(procedure.equals("2 Person Procedure"))
            return "2PersonProcedure";

        else if(procedure.equals("3 Person Procedure"))
            return "3PersonProcedure";

        else if(procedure.equals("4 Person Procedure"))
            return "4PersonProcedure";

        else
            return "5PersonProcedure";
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;

        if (spinner.getId() == R.id.customerProcedureSpinner) {
            selectedProcedure = getProcedure((String) parent.getItemAtPosition(position));
        }

        if (spinner.getId() == R.id.customerCountrySpinner) {
            selectedCountry = (String) parent.getItemAtPosition(position);
            updateStateSpinner(selectedCountry);
        }
        if (spinner.getId() == R.id.customerStateProvinceSpinner) {
            selectedState = (String) parent.getItemAtPosition(position);
        }
    }

    private void updateStateSpinner(String country) {
        if (country.equals("USA"))
            stateSpinnerArray = getResources().getStringArray(R.array.usa_states_array);

        else if (country.equals("CANADA"))
            stateSpinnerArray = getResources().getStringArray(R.array.canada_state_array);

        else
            stateSpinnerArray = getResources().getStringArray(R.array.mexico_state_array);

        stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateSpinnerArray);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        customerStateSpinner.setAdapter(stateAdapter);
        stateAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showMToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private boolean checkValidation() {
        if (customerNameEditText.getText().toString().trim().length() > 0) {
            if (customerLicenseEditText.getText().toString().trim().length() > 0) {
                if (customerYearEditText.getText().toString().trim().length() > 0) {
                    if (customerMakeIdEditText.getText().toString().trim().length() > 0) {
                        if (customerModelIdEditText.getText().toString().trim().length() > 0) {
                            if (customerMileageEditText.getText().toString().trim().length() > 0) {
                                return true;
                            } else {
                                showMToast("Enter Mileage");
                                return false;
                            }
                        } else {
                            showMToast("Enter Model Id");
                            return false;
                        }
                    } else {
                        showMToast("Enter MakeId");
                        return false;
                    }
                } else {
                    showMToast("Enter Year");
                    return false;
                }
            } else {
                showMToast("Enter License");
                return false;
            }

        } else {
            showMToast("Enter Customer Name");
            return false;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
           /* case R.id.cancelButton:
                finish();
                break;*/

            case R.id.saveButton:

                customerName = customerNameEditText.getText().toString();

                if (Connectivity.isConnected(CustomerInfoActivity.this)) {

                    if (checkValidation()) {
                        DashboardActivity.USER_NAME = customerName;

                        if (progressDialog.isShowing())
                            progressDialog.show();

                        if (Constants.CURRENT_VIN.length() == 17)
                            Constants.CURRENT_VIN = customerVINEditText.getText().toString();

                        //  if (!(Utilities.getCustomerInfo(Constants.CURRENT_VIN ) == null))
                        {
                            String url = CherryAPI.ROOT_URL + CherryAPI.SAVE_CUSTOMER_INFO;


                            Map<String, String> map = new HashMap<>();
                            map.put(Constants.SAVE_CUSTOMER_CUSTOMERID, LoginActivity.loginBean.getData().getUser().getId());
                            map.put(Constants.SAVE_CUSTOMER_CARID, "1");
                            map.put(Constants.SAVE_CUSTOMER_NAME, customerNameEditText.getText().toString());
                            map.put(Constants.SAVE_CUSTOMER_STATE, selectedState);
                            map.put(Constants.SAVE_CUSTOMER_COUNTRY, selectedCountry);
                            map.put(Constants.SAVE_CUSTOMER_LICENSE, customerLicenseEditText.getText().toString());
                            map.put(Constants.SAVE_CUSTOMER_BRANCHID, LoginActivity.loginBean.getData().getUserBranch().get(0).getId());
                            map.put(Constants.SAVE_CUSTOMER_CREATEDBY, LoginActivity.loginBean.getData().getUser().getId());

                            if (Constants.CURRENT_VIN.length() == 17)
                                map.put(Constants.SAVE_CUSTOMER_VIN, customerVINEditText.getText().toString());

                            else
                                map.put(Constants.SAVE_CUSTOMER_VIN, Constants.CURRENT_VIN);

                            map.put(Constants.SAVE_CUSTOMER_MAKEID, customerMakeIdEditText.getText().toString());
                            map.put(Constants.SAVE_CUSTOMER_MODELID, customerModelIdEditText.getText().toString());
                            map.put(Constants.SAVE_CUSTOMER_YEAR, customerYearEditText.getText().toString());
                            map.put(Constants.SAVE_CUSTOMER_MILEAGE, customerMileageEditText.getText().toString());

                            GsonRequest<SearchVinBean> customerInfoGsonRequest = new GsonRequest<SearchVinBean>(
                                    Request.Method.POST,
                                    url,
                                    SearchVinBean.class, map,
                                    new com.android.volley.Response.Listener<SearchVinBean>() {
                                        @Override
                                        public void onResponse(SearchVinBean res) {

                                            progressDialog.dismiss();
                                            if (res.getStatus()) {

                                                //Save Selected Procedure for Editing
                                                if (!(res.getData().get(0) == null)) {
                                                    SearchVinBean.Data data = res.getData().get(res.getData().size() - 1);
                                                    data.setCreated_at(selectedProcedure);
                                                    DashboardActivity.selectedProcedure = selectedProcedure;
                                                    data.setModel_id(customerModelIdEditText.getText().toString());
                                                    data.setMake_id(customerMakeIdEditText.getText().toString());
                                                    Utilities.saveCustomerInfoHashMap(Constants.CURRENT_VIN, data);
                                                }
                                                if (!(Utilities.getTempArrayList() == null))
                                                    Utilities.clearTempArrayList();

                                                startActivity(new Intent(CustomerInfoActivity.this, InspectionsToCompleteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(Constants.INSPECTIONS_LIST_FROM, Constants.FROM_CUSTOMER_INFO));

                                            } else
                                                Toast.makeText(CustomerInfoActivity.this,
                                                        "Try Again.",
                                                        Toast.LENGTH_SHORT).show();

                                        }
                                    },
                                    new com.android.volley.Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            progressDialog.dismiss();
                                            Utilities.serverError(CustomerInfoActivity.this);
                                        }
                                    });
                            customerInfoGsonRequest.setShouldCache(false);
                            Utilities.getRequestQueue(CustomerInfoActivity.this).add(customerInfoGsonRequest);

                        }
                      /*  else
                        {
                            if (!(Utilities.getTempArrayList() == null))
                                Utilities.clearTempArrayList();

                            //Save Selected Procedure for Editing
                            SearchVinBean.Data data = null;
                            if (Constants.CUSTOMER_INFO_FROM.equals(Constants.FROM_SCAN_VIN)) {
                                data = ScanVINActivity.searchVinBean.getData().get(0);
                                data.setCreated_at(selectedProcedure);
                            } else {
                                data = PendingInspectionsActivity.searchBeanPendingInspection;
                                data.setCreated_at(selectedProcedure);
                            }
                            Utilities.saveCustomerInfoHashMap(Constants.CURRENT_VIN, data);

                            startActivity(new Intent(CustomerInfoActivity.this, InspectionsToCompleteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(Constants.INSPECTIONS_LIST_FROM, Constants.FROM_CUSTOMER_INFO));

                        }*/

                    }

                } else
                    Utilities.internetConnectionError(CustomerInfoActivity.this);

                break;
        }
    }
}
