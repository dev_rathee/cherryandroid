package cherry.android.com.cherry;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;

import net.authorize.Environment;
import net.authorize.Merchant;
import net.authorize.aim.cardpresent.MarketType;
import net.authorize.auth.PasswordAuthentication;
import net.authorize.auth.SessionTokenAuthentication;
import net.authorize.data.Address;
import net.authorize.data.Customer;
import net.authorize.data.Order;
import net.authorize.data.OrderItem;
import net.authorize.data.ShippingCharges;
import net.authorize.data.creditcard.CreditCard;
import net.authorize.data.mobile.MobileDevice;
import net.authorize.mobile.Result;
import net.authorize.mobile.Transaction;
import net.authorize.mobile.TransactionType;
import net.authorize.util.Luhn;

import java.math.BigDecimal;

import Utils.AnetSingleton;
import Utils.Constants;
import Utils.CreditCardObject;

public class AuthorizeNetIntentService extends IntentService {


    public AuthorizeNetIntentService() {
        super("AuthorizeNetIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        callAuthorizeNet();
    }

    public ShippingCharges createShippingCharges() {
        ShippingCharges shippingCharges = ShippingCharges.createShippingCharges();
        shippingCharges.setTaxAmount(new BigDecimal(0.0));
        shippingCharges.setTaxItemName("Sales Tax");
        shippingCharges.setFreightAmount(new BigDecimal(0.0));
        shippingCharges.setFreightItemName("Shipping and Handling");
        return shippingCharges;
    }

    private CreditCard createCreditCard(CreditCardObject creditCardObject) {
        CreditCard creditCard = net.authorize.data.creditcard.CreditCard.createCreditCard();
        creditCard.setCreditCardNumber(creditCardObject.getCardNumber());
        creditCard.setCardCode(creditCardObject.getSecurityCode());
        creditCard.setExpirationMonth(creditCardObject.getExpMonth());
        creditCard.setExpirationYear(creditCardObject.getExpYear());
        creditCard.setAnetDuplicatemaskedCardNumber(creditCardObject.getCardNumber().substring(12, 16));
        return creditCard;
    }

    private Customer createCustomer() {
        Customer testCustomer = Customer.createCustomer();
        Address billingAddress = Address.createAddress();
        billingAddress.setZipPostalCode(ChangeSubscriptionActivity.contactInfoBean.getData().getZipcode());
        billingAddress.setFirstName(ChangeSubscriptionActivity.contactInfoBean.getData().getFname());
        billingAddress.setLastName(ChangeSubscriptionActivity.contactInfoBean.getData().getLname());
        billingAddress.setAddress(ChangeSubscriptionActivity.contactInfoBean.getData().getAddress());
        billingAddress.setCity(ChangeSubscriptionActivity.contactInfoBean.getData().getCity());
        billingAddress.setCountry(ChangeSubscriptionActivity.contactInfoBean.getData().getCountry());
        billingAddress.setState(ChangeSubscriptionActivity.contactInfoBean.getData().getState());
        testCustomer.setBillTo(billingAddress);
        testCustomer.setShipTo(billingAddress);
        return testCustomer;
    }

    private Order createOrder(BigDecimal totalAmount) {
        Order testOrder = Order.createOrder();
        testOrder.setTotalAmount(totalAmount);
        OrderItem testItem = OrderItem.createOrderItem();
        testItem.setItemId("testItemID");
        testItem.setItemName("testItemName");
        testItem.setItemDescription("testItemDescription");
        testItem.setItemQuantity(new BigDecimal(1));
        testItem.setItemTaxable(false);
        testOrder.addOrderItem(testItem);
        testOrder.setPurchaseOrderNumber("9999");
        return testOrder;
    }

    private String getDeviceId() {
        TelephonyManager telephonyManager = (TelephonyManager)
                getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            return telephonyManager.getDeviceId();
        }
        return "";
    }

    private boolean authenicateUser() {
        final String loginId = "Devrath123";
        final String password = "Devrath@123";
        String deviceId = getDeviceId();

        MobileDevice mobileDevice = MobileDevice.createMobileDevice
                (deviceId, "", "");

        PasswordAuthentication authentication = PasswordAuthentication.createMerchantAuthentication(loginId, password, deviceId);
        AnetSingleton.merchant = Merchant.createMerchant(Environment.SANDBOX, authentication);
        AnetSingleton.merchant.setDuplicateTxnWindowSeconds(30);
        Transaction transaction = AnetSingleton.merchant.createMobileTransaction(TransactionType.MOBILE_DEVICE_LOGIN);
        transaction.setMobileDevice(mobileDevice);
        Result loginResult = (Result) AnetSingleton.merchant.postTransaction(transaction);

        if (loginResult.isOk()) {
            updateSessionToken(loginResult, deviceId);
            return true;
        } else
            return false;
    }

    private void updateSessionToken(net.authorize.xml.Result loginResult, String deviceId) {
        AnetSingleton.merchant.setMerchantAuthentication(SessionTokenAuthentication.createMerchantAuthentication
                (AnetSingleton.merchant.getMerchantAuthentication().getName(), loginResult.getSessionToken(), deviceId));
    }

    private void callAuthorizeNet() {
        try {
            if (authenicateUser()) {
                AnetSingleton.merchant.setMarketType(MarketType.RETAIL);
                String cardNumber = ChangeSubscriptionActivity.contactInfoBean.getData().getCard_no(),
                        cardCVV = ChangeSubscriptionActivity.contactInfoBean.getData().getCard_security_code(),
                        cardExpMonth, cardExpYear;
                String cardExpiration = ChangeSubscriptionActivity.contactInfoBean.getData().getCard_expiry_date();

                String[] expiryArray = cardExpiration.split("/");
                cardExpMonth = expiryArray[0];
                cardExpYear = expiryArray[1];


                if (Luhn.isCardValid(cardNumber)) {
                    CreditCardObject creditCardObject = new CreditCardObject(cardNumber, cardCVV,cardExpMonth, cardExpYear);
                    final BigDecimal totalAmount = new BigDecimal(ChangeSubscriptionActivity.totalBillingPrice + "");

                    Order testOrder = createOrder(totalAmount);

                    net.authorize.aim.Transaction transaction = AnetSingleton.merchant.createAIMTransaction
                            (net.authorize.TransactionType.AUTH_CAPTURE, testOrder.getTotalAmount());

                    transaction.setCreditCard(createCreditCard(creditCardObject));
                    transaction.setShippingCharges(createShippingCharges());
                    transaction.setOrder(createOrder(totalAmount));
                    transaction.setCustomer(createCustomer());
                    net.authorize.aim.Result transactionResult = (net.authorize.aim.Result)
                            AnetSingleton.merchant.postTransaction(transaction);

                    if (transactionResult != null && transactionResult.isOk()) {
                        String transactionAmount = transactionResult.getRequestTransaction().getOrder().
                                getTotalAmount().toString();
                        String accountNumber = transactionResult.getAccountNumber();
                        String accountTypeName = transactionResult.getAccountType().name();
                        String transactionId = transactionResult.getTransId();

                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(Constants.SUBSCRIPTION_INTENT_FILTER).putExtra(Constants.SUBSCRIPTION_MESSAGE,"Transaction successful").putExtra(Constants.TRANSACTION_ID,transactionId));

                    }
                    else
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(Constants.SUBSCRIPTION_INTENT_FILTER).putExtra(Constants.SUBSCRIPTION_MESSAGE,"Transaction failed"));

                } else
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(Constants.SUBSCRIPTION_INTENT_FILTER).putExtra(Constants.SUBSCRIPTION_MESSAGE,"Invalid Card Number"));


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
