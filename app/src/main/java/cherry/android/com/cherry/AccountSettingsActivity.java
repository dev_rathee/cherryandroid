package cherry.android.com.cherry;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AccountSettingsActivity extends AppCompatActivity implements View.OnClickListener{

    Button backButton,saveButton,saveTextButton;
    TextView headerTextView;
    RelativeLayout contactInfoRelativeLayout,loginSettingsRelativeLayout,paymentDetailsRelativeLayout,yourSubscriptionRelativeLayout;
    private final String SUBSCRIPTION_TYPE = "SUBSCRIPTION_TYPE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        initialiseUI();


    }

    private void initialiseUI() {

        backButton = (Button) findViewById(R.id.cancelButton);
        backButton.setOnClickListener(this);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setVisibility(View.GONE);

        saveTextButton = (Button) findViewById(R.id.saveTextButton);
        saveTextButton.setVisibility(View.GONE);

        headerTextView = (TextView) findViewById(R.id.headerTextView);
        headerTextView.setText("Account Settings");

        contactInfoRelativeLayout = (RelativeLayout) findViewById(R.id.contactInfoRelativeLayout);
        contactInfoRelativeLayout.setOnClickListener(this);

        loginSettingsRelativeLayout = (RelativeLayout) findViewById(R.id.loginSettingsRelativeLayout);
        loginSettingsRelativeLayout.setOnClickListener(this);

        yourSubscriptionRelativeLayout = (RelativeLayout) findViewById(R.id.yourSubscriptionRelativeLayout);
        yourSubscriptionRelativeLayout.setOnClickListener(this);

        paymentDetailsRelativeLayout = (RelativeLayout) findViewById(R.id.paymentDetailsRelativeLayout);
        paymentDetailsRelativeLayout.setOnClickListener(this);
    }

    private void showSubscriptionOptions()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select one Subscription from below");
        builder.setItems(new CharSequence[]
                        {"Store Subscription", "Device Subscription", "Cancel"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                startActivity(new Intent(AccountSettingsActivity.this,SubscriptionActivity.class).putExtra(SUBSCRIPTION_TYPE,"1"));
                                break;
                            case 1:
                                startActivity(new Intent(AccountSettingsActivity.this,SubscriptionActivity.class).putExtra(SUBSCRIPTION_TYPE,"2"));
                                break;


                        }
                    }
                });
        builder.create().show();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.cancelButton :
                finish();
                break;


            case R.id.contactInfoRelativeLayout :
                startActivity(new Intent(AccountSettingsActivity.this,ContactInfoProfile.class));
                break;

            case R.id.loginSettingsRelativeLayout :
                startActivity(new Intent(AccountSettingsActivity.this,LoginSettingsActivity.class));
                break;

            case R.id.yourSubscriptionRelativeLayout :
                 showSubscriptionOptions();
                break;

            case R.id.paymentDetailsRelativeLayout :
                startActivity(new Intent(AccountSettingsActivity.this,PaymentDetailsActivity.class));
                break;
        }
    }
}
