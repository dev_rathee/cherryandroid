
package cherry.android.com.cherry;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapters.PendingInspectionsAdapter;
import Utils.Constants;
import Utils.Utilities;
import beans.SearchVinBean;


public class PendingInspectionsActivity extends ActionBarActivity implements View.OnClickListener {

    static SearchVinBean.Data searchBeanPendingInspection;
    Button cancelButton, saveButton;
    TextView headerTextView, noPendingInspectionsTextView;
    RecyclerView pendingInspectionsRecyclerView;
    PendingInspectionsAdapter pendingInspectionsAdapter;
    ArrayList<SearchVinBean.Data> customerInfoArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_inspections);

        initialiseUI();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.saveButton) {
            if (v.getId() == R.id.saveButton)
                startActivity(new Intent(PendingInspectionsActivity.this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

        }

        if (v.getId() == R.id.newCustomerRelativeLayout)
            startActivity(new Intent(PendingInspectionsActivity.this, ScanVINActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

        else if (v.getId() == R.id.branchRelativeLayout) {
            //  startActivity(new Intent(DashboardActivity.this,CustomerInfoActivity.class));
        } else if (v.getId() == R.id.inspectionsRelativeLayout)
            startActivity(new Intent(PendingInspectionsActivity.this, PendingInspectionsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

        else if (v.getId() == R.id.logoutButton)
            startActivity(new Intent(PendingInspectionsActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

    }

    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int i) {

            try {

                Button settingButton = (Button) viewHolder.itemView.findViewById(R.id.settingButton);
                settingButton.setVisibility(View.VISIBLE);
                settingButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Constants.CUSTOMER_INFO_FROM = Constants.FROM_PENDING_INSPECTION;
                        DashboardActivity.USER_NAME = customerInfoArrayList.get(viewHolder.getAdapterPosition()).getName();
                        Constants.CURRENT_VIN = customerInfoArrayList.get(viewHolder.getAdapterPosition()).getVin();
                        searchBeanPendingInspection = customerInfoArrayList.get(viewHolder.getAdapterPosition());
                        DashboardActivity.selectedProcedure = customerInfoArrayList.get(viewHolder.getAdapterPosition()).getCreated_at();
                        startActivity(new Intent(PendingInspectionsActivity.this, CustomerInfoActivity.class));
                    }
                });

                pendingInspectionsAdapter.notifyDataSetChanged();


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            return false;
        }
    };


    private void initialiseUI() {

        try {

            headerTextView = (TextView) findViewById(R.id.headerTextView);
            headerTextView.setText("Inspections to Complete");

            noPendingInspectionsTextView = (TextView) findViewById(R.id.pendingInspectionsTextView);

            saveButton = (Button) findViewById(R.id.saveButton);
            saveButton.setText("");
            saveButton.setOnClickListener(this);

            cancelButton = (Button) findViewById(R.id.cancelButton);
            cancelButton.setVisibility(View.GONE);

            pendingInspectionsRecyclerView = (RecyclerView) findViewById(R.id.pendingInspectionListView);

            if (customerInfoArrayList == null)
                customerInfoArrayList = new ArrayList<>();

            customerInfoArrayList.clear();

            HashMap<String, SearchVinBean.Data> pendingHashMap = Utilities.customerInfoHashMap;

            if (!(pendingHashMap == null)) {
                for (Map.Entry<String, SearchVinBean.Data> entry : pendingHashMap.entrySet()) {
                    customerInfoArrayList.add(entry.getValue());
                }

                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);

                if (customerInfoArrayList.size() > 0) {
                    noPendingInspectionsTextView.setVisibility(View.GONE);
                    pendingInspectionsAdapter = new PendingInspectionsAdapter(this, customerInfoArrayList);
                    pendingInspectionsRecyclerView.setAdapter(pendingInspectionsAdapter);
                    itemTouchHelper.attachToRecyclerView(pendingInspectionsRecyclerView);
                    pendingInspectionsRecyclerView.setHasFixedSize(true);

                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
                    pendingInspectionsRecyclerView.setLayoutManager(mLayoutManager);
                    pendingInspectionsAdapter.notifyDataSetChanged();

                } else {
                    noPendingInspectionsTextView.setVisibility(View.VISIBLE);

                    customerInfoArrayList.clear();
                    pendingInspectionsAdapter = new PendingInspectionsAdapter(this, customerInfoArrayList);
                    pendingInspectionsRecyclerView.setAdapter(pendingInspectionsAdapter);
                    pendingInspectionsRecyclerView.setHasFixedSize(true);

                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
                    pendingInspectionsRecyclerView.setLayoutManager(mLayoutManager);
                    pendingInspectionsAdapter.notifyDataSetChanged();
                }
            } else {
                noPendingInspectionsTextView.setVisibility(View.VISIBLE);

                pendingInspectionsAdapter = new PendingInspectionsAdapter(this, customerInfoArrayList);
                pendingInspectionsRecyclerView.setAdapter(pendingInspectionsAdapter);
                pendingInspectionsRecyclerView.setHasFixedSize(true);

                LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
                pendingInspectionsRecyclerView.setLayoutManager(mLayoutManager);
                pendingInspectionsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
